﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : Singleton<WaveManager>
{
    public WaveController waveControllerPrefab;

    public List<WaveSO> wavesData;
    public List<WaveSO> wavePowerUp;
    public List<WaveSO> waveItemArmor;
    public List<WaveSO> waveItemSkill;

    public WaveController leftControl;
    public WaveController rightControl;
    Queue<WaveSO> mainQueue = new Queue<WaveSO>();
    Queue<WaveSO> leftQueue = new Queue<WaveSO>();
    Queue<WaveSO> rightQueue = new Queue<WaveSO>();

    protected bool isFirstWave = true;
    public bool IsReady() { return isFirstWave; }
    public void CreateWave()
    {
        ///Giả sử hiện tại mỗi  10 wave
        ///wave thứ 3 và 7 là wave powerup
        ///wave thứ 9 là wave skill
        ///random 2 wave là armor break nhưng hiện tại chưa có animation nên chưa thể làm đc
        for (int i = 0; i < 10; i++)
        {

            int random = Random.Range(0, wavesData.Count);
            mainQueue.Enqueue(wavesData[random]);
        }
    }
    void FetchWaveToQueue()
    {
        for (int i = 0; i < 5; i++)
        {
            var data = mainQueue.Dequeue();
            leftQueue.Enqueue(data);
            rightQueue.Enqueue(data);
        }
        if (mainQueue.Count < 6)
        {
            CreateWave();
        }
       
    }
    public WaveSO FetchWave(Side side)
    {
        WaveSO data;
        if(side == Side.Left)
        {
            data = leftQueue.Dequeue();
        }
        else 
        {
            data = rightQueue.Dequeue();
        }
        if (leftQueue.Count <= 3 || rightQueue.Count <= 3)
        {
            FetchWaveToQueue();
        }
        return data;
    }
    void SetDataForWaveController()
    {
        isFirstWave = true;
        leftControl = Instantiate(waveControllerPrefab, transform);
        rightControl = Instantiate(waveControllerPrefab, transform);
        
        leftControl.SetData(Side.Left, OnWaveControllerReady);  
        rightControl.SetData(Side.Right, OnWaveControllerReady);
    }
    void FetchWaveForWaveController()
    {
        isFirstWave = true;
        leftControl.FetchWaveAndSetData();
        rightControl.FetchWaveAndSetData();
    }
    void OnWaveControllerReady(WaveController control)
    {
        if (!isFirstWave)
        {
            control.ActionSpawnWave();
        }
    }
    public void PrepareWave()
    {
        mainQueue.Clear();
        leftQueue.Clear();
        rightQueue.Clear();

        CreateWave();
        FetchWaveToQueue();
        SetDataForWaveController();
        FetchWaveForWaveController();
    }
    public void StartToSpawn()
    {
        leftControl.ActionSpawnWave();
        rightControl.ActionSpawnWave();
        isFirstWave = false;
    }
    private void OnGUI()
    {
        if (GUI.Button(new Rect(100, 10, 100, 50), "Start game normal"))
        {
            PrepareWave();
            StartToSpawn();
        }
    }
}
