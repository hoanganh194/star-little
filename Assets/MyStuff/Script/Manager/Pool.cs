﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Pool : Singleton<Pool>
{
    public List<PreloadTransform> preloadData;
    Dictionary<string, Stack<Transform>> pool;
    public void StartPool()
    {
        pool = new Dictionary<string, Stack<Transform>>();
        InitializedPoolData();
    }
    public void Despawn(string poolName, Transform objectTransform)
    {
        objectTransform = EditDespawnTranform(objectTransform);
        if (!pool.ContainsKey(poolName))
        {
            Debug.LogError("PoolDespawn: Không tồn tại pool name: " + poolName);
            Destroy(objectTransform.gameObject);
            return;
        }
        if (pool[poolName].Contains(objectTransform))
        {
            Debug.LogError($"PoolDespawn: Đã có {objectTransform.name} trong " + poolName);
            Destroy(objectTransform.gameObject);
            return;
        }

        //Có thể thêm chức năng kiểm tra loại object đúng không sau này
        pool[poolName].Push(objectTransform);

    }
    public Transform Spawn(string poolName, bool force = true)
    {
        if(!pool.ContainsKey(poolName))
        {
            Debug.LogError("PoolSpawn: Không tồn tại pool name: " + poolName);
            return null;
        }
        if (pool[poolName].Count == 0)
        {
            if (force)
            {
                foreach (var data in preloadData)
                {
                    if (data.poolName == poolName)
                    {
                        Debug.Log("PoolSpawn: Không đủ item trong pool => tạo mới: " + poolName);
                        Transform addingData = Instantiate(data.prefab, transform);
                        addingData = EditSpawnTranform(addingData);
                        return addingData;
                    }
                }
            }
            return null;
        }
        return EditSpawnTranform(pool[poolName].Pop());
    }
    public void CreatePool(string name, Transform objectTransform, int numberAllow = -1)
    {
        if (pool.ContainsKey(name))
        {
            pool[name].Clear();
            for (int i = 0; i < numberAllow; i++)
            {
                var addTranform = EditDespawnTranform(Instantiate(objectTransform, transform));
                pool[name].Push(addTranform);
            }
            return;
        }
        
        Stack<Transform> newStack = new Stack<Transform>();
        if (numberAllow <= 0)
        {
            Debug.Log("PoolCreate: pool có số lượng = 0 -> chỉ khởi tạo");
        }
        for (int i = 0; i < numberAllow; i++)
        {
            var addTranform = EditDespawnTranform(Instantiate(objectTransform, transform));
            newStack.Push(addTranform);
        }
        pool.Add(name, newStack);
    }
    void InitializedPoolData()
    {
        foreach (var data in preloadData)
        {
            Stack<Transform> newStack = new Stack<Transform>();
            for(int i = 0; i < data.value; i++)
            {
                Transform addingData = EditDespawnTranform(Instantiate(data.prefab, transform));
                newStack.Push(addingData);
            }
            pool.Add(data.poolName, newStack);
        }
    }
    Transform EditSpawnTranform(Transform objectTransform)
    {
        var entity = objectTransform.GetComponent<Entity>();
        if (entity)
        {
            entity.SetActiveRootChild(false);
        }
        objectTransform.gameObject.SetActive(true);
        return objectTransform;
    }
    Transform EditDespawnTranform(Transform objectTransform)
    {
        var entity = objectTransform.GetComponent<Entity>();
        if (entity)
        {
            entity.SetActiveRootChild(false);
        }
        objectTransform.gameObject.SetActive(false);
        return objectTransform;
    }
}


[Serializable]
public class PreloadTransform
{
    public string poolName;
    public Transform prefab;
    public int value;
}

[Serializable]
public class TestDataPool
{
    public string poolName;
    public int value;
}
