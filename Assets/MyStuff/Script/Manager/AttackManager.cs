﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class AttackManager : Singleton<AttackManager>
{
    System.Action SpawnSpecialAttackLeft;
    System.Action SpawnSpecialAttackRight;

    public float delay = 0.5f;
    public PathGenerator pathGenerator = new PathGenerator();
    int comboLeft = 0, comboRight = 0;
    public void CreateSpecialAttackPool(SpecialAttack left, SpecialAttack right)
    {
        Pool.Instance.CreatePool(Utils.PoolName(AttackType.Special, Side.Left), left.transform, 10);
        Pool.Instance.CreatePool(Utils.PoolName(AttackType.Special, Side.Right), right.transform, 10);
    }
    /// <summary>
    /// Type of Path : 0 - Chạm tường || 1 - Đồ thị bậc 2 || 2 - Chữ U ngược  ||  3 - Đi chéo lên rồi thẳng xuống || 4 - Đi thẳng tới mục tiêu
    /// </summary>
    public VertexPath GetNormalAttackPath(Vector3 myPosition , Vector3 targetPosition, int typeOfPath = -1, Wall wall = Wall.Top)
    {
        VertexPath path = null;
        if (typeOfPath < 0)
        {
            typeOfPath = Random.Range(0, 4);
        }
        switch (typeOfPath)
        {
            case 0:
                path = pathGenerator.GenerateVextrexPathWall(myPosition, targetPosition, wall);
                break;
            case 1:
                path = pathGenerator.GeneraterPathMatrixAddone(myPosition, targetPosition);
                break;
            case 2:
                path = pathGenerator.GenerateVextrexPathUpDown(myPosition, targetPosition);
                break;
            case 3:
                path = pathGenerator.GenerateVextrexPathUpDownLocal(myPosition, targetPosition);
                break;
            case 4:
                path = pathGenerator.GenerateVextrexPathForward(myPosition, targetPosition);
                break;
            case 5:
                path = pathGenerator.GeneraterPathMatrixAddoneReverse(myPosition, targetPosition);
                break;
        }
        return path;
    }
    void SpawnAttackTest()
    {
        //var attack = Pool.Instance.Spawn(Utils.poolNameReverseAttack).GetComponent<ReverseAttack>();
        //var path = pathGenerator.GenerateVextrexPathWall(GameplayManager.Instance.playerManager.GetPlayer(Side.Left).transform.position, GameplayManager.Instance.playerManager.GetPlayer(Side.Right).transform.position);
        ////var path = pathGenerator.GeneraterPathMatrixAddone(GameplayManager.Instance.GetPlayer(Side.Left).myPlayer.transform.position, GameplayManager.Instance.GetPlayer(Side.Right).myPlayer.transform.position);
        ////var path = pathGenerator.GenerateVextrexPathForward(GameplayManager.Instance.GetPlayer(Side.Left).myPlayer.transform.position, GameplayManager.Instance.GetPlayer(Side.Right).myPlayer.transform.position);
        //attack.SetDataTest(Side.Left,Size.Five, path, 10);
        var poolName = Utils.PoolName(AttackType.Special, Side.Left);
        var opponentSide = Utils.GetOpponentSide(Side.Left);
        var attack = Pool.Instance.Spawn(poolName).GetComponent<SpecialAttack>();
        attack.SetData(opponentSide, GameplayManager.Instance.playerManager.GetPlayer(Side.Left).transform.position, poolName);
    }
    private void OnGUI()
    {
        if (GUI.Button(new Rect(250, 10, 100, 50), "Spawn Attack"))
        {
            SpawnAttackTest();
        }
    }
    #region Creep Die
    void OnCreepDeath(Creep creep)
    {
        switch (creep.Side)
        {
            case Side.Left:
                ComboCounterForLeft(creep);
                break;
            case Side.Right:
                ComboCounterForRight(creep);
                break;
        }
    }
    void ComboCounterForLeft(Creep creep)
    {
        if (Time.deltaTime <= delay)
        {
            comboLeft++;
        }
        else
        {
            comboLeft = 1;
        }

        if (comboLeft >= 5)
        {
            comboLeft = 0;
            SpawnAttackNormalAttack(Side.Right, creep);
        }
    }
    void ComboCounterForRight(Creep creep)
    {
        if (Time.deltaTime <= delay)
        {
            comboRight++;
        }
        else
        {
            comboRight = 1;
        }

        if (comboRight >= 5)
        {
            comboLeft = 0;
            SpawnAttackNormalAttack(Side.Left, creep);
        }
    }
    void SpawnAttackNormalAttack(Side side, Creep creep)
    {
        var attack = Pool.Instance.Spawn(Utils.poolNameNormalAttack).GetComponent<NormalAttack>();
        attack.SetData(side, creep.mySize, Random.Range(3f, 10f), creep.myPrevPos);
    }
    #endregion
    #region Attack Die
    void OnAttackDie(Attack attack)
    {
        switch (attack.attackType)
        {
            case AttackType.Normal:
                SpawnReserveAttack(attack);
                break;
            case AttackType.Reverse:
                SpawnSpecialAttack(attack);
                break;
            default:
                Debug.Log($"{attack.attackType} side {attack.Side} death");
                break;
        }
    }
    void SpawnReserveAttack(Attack normalAttack)
    {
        var attack = Pool.Instance.Spawn(Utils.poolNameReverseAttack).GetComponent<ReverseAttack>();
        attack.SetData(Utils.GetOpponentSide(normalAttack.Side), Utils.GetForwardSide(normalAttack.mySize), Random.Range(3f, 10f), normalAttack.myPrevPos);
    }
    void SpawnSpecialAttack(Attack normalAttack)
    {
        var opponentSide = Utils.GetOpponentSide(normalAttack.Side);
        var poolName = Utils.PoolName(AttackType.Special, normalAttack.Side);
        var attack = Pool.Instance.Spawn(poolName).GetComponent<SpecialAttack>();
        attack.SetData(opponentSide, normalAttack.myPrevPos, poolName);
    }
    #endregion
    #region Event listener
    private void Start()
    {
        EventsManager.Instance.onCreepDeath += OnCreepDeath;
        EventsManager.Instance.onAttackDeath += OnAttackDie;
    }
    private void OnDisable()
    {
        EventsManager.Instance.onCreepDeath -= OnCreepDeath;    
        EventsManager.Instance.onAttackDeath -= OnAttackDie;
    }
    public void AssignSpecialAttackSpawn(Side side, System.Action actionSpawn)
    {
        switch (side)
        {
            case Side.Left:
                SpawnSpecialAttackLeft = actionSpawn;
                break;

            case Side.Right:
                SpawnSpecialAttackRight = actionSpawn;
                break;
        }
    }
    #endregion
}
