﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class PathGenerator
{
    /// <summary>
    /// Path theo đồ thị bậc 2 từ điểm bắt đầu đến kết thúc từ trên xuống (ưu tiên hình U ngược max là cao nhất)
    /// </summary>
    public VertexPath GeneraterPathMatrixAddone(Vector2 sta, Vector2 end)
    {
        Vector2 mid = new Vector2((sta.x + end.x)/2, 5);
        float max;
        Vector3 dataPath;
        do
        {
            mid.y -= 0.5f;
            dataPath = GeneraterGraphWithMatrix(sta, mid, end);
            float delta = -dataPath.y / (2 * dataPath.x);
            max = dataPath.x * delta * delta + dataPath.y * delta + dataPath.z;
        }
        while (max > 4.5f);

        float deltaX = (end.x - sta.x) / 20;
        float index = sta.x;
        List<Vector2> waypoints = new List<Vector2>();
        while (true)
        {
            var x = index;
            var y = dataPath.x * x * x + dataPath.y * x + dataPath.z;
            if (Mathf.Abs(y) > Utils.maxPosY + 2 || Mathf.Abs(x) > Utils.maxPosX + 2 ) break;

            waypoints.Add(new Vector2(x, y));
            index += deltaX;
        }
        Vector2[] waypointsArray = waypoints.ToArray();
        VertexPath vertexPath = null;
        if (waypointsArray.Length > 0)
        {
            BezierPath bezierPath = new BezierPath(waypointsArray, false, PathSpace.xy);
            bezierPath.AutoControlLength = 0.01f;
            vertexPath = new VertexPath(bezierPath, AttackManager.Instance.transform);
        }
        return vertexPath;
    }
    /// <summary>
    /// Path theo đồ thị bậc 2 từ điểm bắt đầu đến kết thúc từ dưới lên (ưu tiên hình U max thấp nhất)
    /// </summary>
    public VertexPath GeneraterPathMatrixAddoneReverse(Vector2 sta, Vector2 end)
    {
        Vector2 mid = new Vector2((sta.x + end.x) / 2, -5);
        float max;
        Vector3 dataPath;
        do
        {
            mid.y += 0.5f;
            dataPath = GeneraterGraphWithMatrix(sta, mid, end);
            float delta = -dataPath.y / (2 * dataPath.x);
            max = dataPath.x * delta * delta + dataPath.y * delta + dataPath.z;
        }
        while (max < -4.5f);

        float deltaX = (end.x - sta.x) / 20;
        float index = sta.x;
        List<Vector2> waypoints = new List<Vector2>();
        while (true)
        {
            var x = index;
            var y = dataPath.x * x * x + dataPath.y * x + dataPath.z;
            if (Mathf.Abs(y) > Utils.maxPosY + 2 || Mathf.Abs(x) > Utils.maxPosX + 2) break;

            waypoints.Add(new Vector2(x, y));
            index += deltaX;
        }
        Vector2[] waypointsArray = waypoints.ToArray();
        VertexPath vertexPath = null;
        if (waypointsArray.Length > 0)
        {
            BezierPath bezierPath = new BezierPath(waypointsArray, false, PathSpace.xy);
            bezierPath.AutoControlLength = 0.01f;
            vertexPath = new VertexPath(bezierPath, AttackManager.Instance.transform);
        }
        return vertexPath;
    }
    /// <summary>
    /// Path chạm bật tường
    /// </summary>
    public VertexPath GenerateVextrexPathWall(Vector2 sta, Vector2 end, Wall wall = Wall.Top)
    {
        Vector2[] waypoints = new Vector2[4];
        Vector2 wallPoint = new Vector2((sta.x + end.x) / 2, Utils.maxPosY);
        switch (wall)
        {
            case Wall.Left:
                wallPoint = new Vector2(-Utils.maxPosX, (sta.y + end.y) / 2);
                break;
            case Wall.Right:
                wallPoint = new Vector2(Utils.maxPosX, (sta.y + end.y) / 2);
                break;
            case Wall.Bottom:
                wallPoint = new Vector2((sta.x + end.x) / 2, -Utils.maxPosY);
                break;
        }
        waypoints[0] = sta;
        waypoints[1] = wallPoint;
        waypoints[2] = end;
        waypoints[3] = end + (end - wallPoint).normalized * 7;

        VertexPath vertexPath = null;
        if (waypoints.Length > 0)
        {
            BezierPath bezierPath = new BezierPath(waypoints, false, PathSpace.xy);
            bezierPath.AutoControlLength = 0.01f;
            vertexPath = new VertexPath(bezierPath, AttackManager.Instance.transform);
        }
        return vertexPath;
    }
    /// <summary>
    /// Path đi thẳng lên rồi đi ngang rồi đi xuống
    /// </summary>
    public VertexPath GenerateVextrexPathUpDown(Vector2 sta, Vector2 end)
    {
        Vector2[] waypoints = new Vector2[4];
      
        waypoints[0] = sta;
        waypoints[1] = new Vector2(sta.x, Utils.maxPosY);
        waypoints[2] = new Vector2(end.x, Utils.maxPosY);
        waypoints[3] = new Vector2(end.x, -7);

        VertexPath vertexPath = null;
        if (waypoints.Length > 0)
        {
            BezierPath bezierPath = new BezierPath(waypoints, false, PathSpace.xy);
            bezierPath.AutoControlLength = 0.01f;
            vertexPath = new VertexPath(bezierPath, AttackManager.Instance.transform);
        }
        return vertexPath;
    }
    /// <summary>
    /// Path đi thẳng tới vị trí trên đầu mục tiêu rồi đi xuống
    /// </summary>
    public VertexPath GenerateVextrexPathUpDownLocal(Vector2 sta, Vector2 end)
    {
        Vector2[] waypoints = new Vector2[3];

        waypoints[0] = sta;
        waypoints[1] = new Vector2(end.x, Utils.maxPosY);
        waypoints[2] = new Vector2(end.x, -7);

        VertexPath vertexPath = null;
        if (waypoints.Length > 0)
        {
            BezierPath bezierPath = new BezierPath(waypoints, false, PathSpace.xy);
            bezierPath.AutoControlLength = 0.01f;
            vertexPath = new VertexPath(bezierPath, AttackManager.Instance.transform);
        }
        return vertexPath;
    }
    /// <summary>
    /// Path thẳng từ vị trí start đến end.
    /// </summary>
    public VertexPath GenerateVextrexPathForward(Vector2 sta, Vector2 end)
    {
        Vector2[] waypoints = new Vector2[2];

        waypoints[0] = sta;
        waypoints[1] = end;

        VertexPath vertexPath = null;
        if (waypoints.Length > 0)
        {
            BezierPath bezierPath = new BezierPath(waypoints, false, PathSpace.xy);
            bezierPath.AutoControlLength = 0.01f;
            vertexPath = new VertexPath(bezierPath, AttackManager.Instance.transform);
        }
        return vertexPath;
    }
    #region Internal Method
    Vector3 GeneraterGraphWithMatrix(Vector2 sta, Vector2 mid, Vector2 end)
    {
        float[,] D = new float[3, 3]
        {
            { sta.x*sta.x,  sta.x,  1 },
            { mid.x*mid.x,  mid.x,  1 },
            { end.x*end.x,  end.x,  1 }
        };
        float[,] D1 = new float[3, 3]
        {
            { sta.y,  sta.x,  1 },
            { mid.y,  mid.x,  1 },
            { end.y,  end.x,  1 }
        };
        float[,] D2 = new float[3, 3]
        {
            { sta.x*sta.x,  sta.y,  1 },
            { mid.x*mid.x,  mid.y,  1 },
            { end.x*end.x,  end.y,  1 }
        };
        float[,] D3 = new float[3, 3]
        {
            { sta.x*sta.x,  sta.x,  sta.y },
            { mid.x*mid.x,  mid.x,  mid.y },
            { end.x*end.x,  end.x,  end.y }
        };
        return new Vector3(detM(D1) / detM(D), detM(D2) / detM(D), detM(D3) / detM(D));
    }
    float detM(float[,] matrix)
    {
        //Debug.Log($"{matrix[0, 0]} : {matrix[0, 1]} : {matrix[0, 2]}");
        //Debug.Log($"{matrix[1, 0]} : {matrix[1, 1]} : {matrix[1, 2]}");
        //Debug.Log($"{matrix[2, 0]} : {matrix[2, 1]} : {matrix[2, 2]}");

        //Debug.Log($"{matrix[0, 0]} * ({matrix[1, 1]} * {matrix[2, 2]} - {matrix[1, 2]} * {matrix[2, 1]})");
        //Debug.Log($"{matrix[0, 1]} * ({matrix[1, 0]} * {matrix[2, 2]} - {matrix[2, 0]} * {matrix[1, 2]})");
        //Debug.Log($"{matrix[0, 2]} * ({matrix[1, 0]} * {matrix[2, 1]} - {matrix[2, 0]} * {matrix[1, 1]})");
        return matrix[0, 0] * (matrix[1, 1] * matrix[2, 2] - matrix[1, 2] * matrix[2, 1])
            - matrix[0, 1] * (matrix[1, 0] * matrix[2, 2] - matrix[2, 0] * matrix[1, 2])
            + matrix[0, 2] * (matrix[1, 0] * matrix[2, 1] - matrix[2, 0] * matrix[1, 1]);
    }
    #endregion

}
