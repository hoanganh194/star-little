﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameManager : Singleton<GameManager>
{
    public GameState gameState { get; private set; }
    public int point { get; private set; }
    public int highestScore { get; private set; }
}
