﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Chưa cần thiết
/// </summary>
public class AudioManager : Singleton<AudioManager>
{
    public AudioSource backgroundPlayer;
    public AudioSource sfxPlayer;
    public AudioSource voidPlayer;
    //Sau này thêm sau
    public void PlayMusic()
    {
        if (backgroundPlayer.isPlaying)
        {
            StartCoroutine(DimThePlayer(backgroundPlayer, 0, 1));
        }
    }
    IEnumerator DimThePlayer(AudioSource player, float value, float time)
    {
        float delta = (player.volume - value) / 5;
        while(player.volume >= value)
        {
            player.volume -= delta;
            yield return Yielders.Get(time / 5);
        }
    }

}
