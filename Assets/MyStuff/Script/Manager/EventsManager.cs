﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class EventsManager : Singleton<EventsManager>
{
    public Action<Creep> onCreepDeath;
    public Action<Attack> onAttackDeath;
    #region Subcribe
    public void OnCreepDeath(Creep creep)
    {
        onCreepDeath?.Invoke(creep);   
    }
    public void OnAttackDeath(Attack attack)
    {
        onAttackDeath?.Invoke(attack);   
    }
    #endregion
}
