﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MyPool : Singleton<MyPool>
{
    Dictionary<string, List<GameObject>> pool = new Dictionary<string, List<GameObject>>();
    public List<PoolData> displayData = new List<PoolData>();
    private void Start()
    {
        InvokeRepeating(nameof(UpdateData), 1f, 0.25f);
    }
    void UpdateData()
    {
        displayData.Clear();
        foreach (var pair in pool)
        {
            var data = new PoolData
            {
                keyWord = pair.Key,
                data = pair.Value
            };
            displayData.Add(data);
        }
    }
    /// <summary>
    /// Return a pool with name
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public List<GameObject> GetPool(string name)
    {
        if (pool.ContainsKey(name))
        {
            return pool[name];
        }
        var newList = new List<GameObject>();
        pool.Add(name, newList);
        return pool[name];
    }
    /// <summary>
    /// Destroy eveything in a pool
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public List<GameObject> ClearPool(string name)
    {
        if (pool.ContainsKey(name))
        {
            pool[name].ForEach(x => Destroy(x));
            pool[name].Clear();
            return pool[name];
        }
        var newList = new List<GameObject>();
        pool.Add(name, newList);
        return pool[name];
    }
    /// <summary>
    /// Destroy a pool with its objects
    /// </summary>
    /// <param name="name"></param>
    public void RemovePool(string name)
    {
        if (pool.ContainsKey(name))
        {
            pool[name].ForEach(x => Destroy(x));
            pool[name].Clear();
            pool.Remove(name);
        }
    }
    /// <summary>
    /// Add a object to pool
    /// </summary>
    /// <param name="name"></param>
    /// <param name="thing"></param>
    public void AddPool(string name,GameObject thing)
    {
        var pool = GetPool(name);
        pool.Add(thing);
    }
    /// <summary>
    /// Remove a single object in pool
    /// </summary>
    /// <param name="name"></param>
    /// <param name="me"></param>
    public void RemoveMe(string name, GameObject me)
    {
        var pool = GetPool(name);
        if (pool.Contains(me))
        {
            pool.Remove(me);
            return;
        }
        Debug.LogError($"Dont have {me.name} at {name} to remove");
    }
}
[System.Serializable]
public class PoolData
{
    public string keyWord;
    public List<GameObject> data;
}

