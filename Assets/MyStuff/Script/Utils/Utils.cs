﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    #region Pool name
    public static string poolNamePlayer = "Player";

    public static string poolNameGhostCreep = "GhostCreep";
    public static string poolNamePathCreep = "PathCreep";
    public static string poolNameNonPathCreep = "NonPathCreep";
    
    public static string poolNameNormalAttack = "NormalAttack";
    public static string poolNameReverseAttack = "ReverseAttack";

    public static string poolNameSpecialAttack = "SpeicalAttack";
    public static string poolNameBossAttack = "BossAttack";

    public static string poolNameNormalBullet = "NormalBullet";
    public static string poolNameChargeBullet = "ChargeBullet";

    public static string poolNameExplosion = "Explosion";

    public static string poolNamePattern = "Pattern";
    public static string poolNamePath = "Path";

    public static string poolLeftSide = "Left";
    public static string poolRightSide = "Right";
    /// <summary>
    /// Các thứ được set pool là: Bullet (những thứ cần kiểm soát số lượng)
    /// </summary>
    /// <param name="charLayer"></param>
    /// <param name="sideLayer"></param>
    /// <returns></returns>
    public static string PoolName(BulletType bulletLayer,Side sideLayer)
    {
        string layer = "";
        string side = "";
        switch (bulletLayer)
        {
            case BulletType.Normal:
                layer = poolNameNormalBullet;
                break;
            case BulletType.Charge:
                layer = poolNameChargeBullet;
                break;
        }
        switch (sideLayer)
        {
            case Side.Left:
                side = poolLeftSide;
                break;
            case Side.Right:
                side = poolRightSide;
                break;
        }
        if (side == "" || layer == "")
        {
            Debug.LogError("Can not return proper pool name");
        }
        return layer + side;
    }
    public static string PoolName(AttackType bulletLayer, Side sideLayer)
    {
        string layer = "";
        string side = "";
        switch (bulletLayer)
        {
            case AttackType.Special:
                layer = poolNameSpecialAttack;
                break;
            case AttackType.Boss:
                layer = poolNameBossAttack;
                break;
        }
        switch (sideLayer)
        {
            case Side.Left:
                side = poolLeftSide;
                break;
            case Side.Right:
                side = poolRightSide;
                break;
        }
        if (side == "" || layer == "")
        {
            Debug.LogError("Can not return proper pool name");
        }
        return layer + side;
    }
    #endregion

    #region Scene Set Up Position
    public static float maxPosX = 6.6667f;
    public static float maxPosY = 5;
    public static float offSet = 0.5f;
    public static float sideColWidth = 0.5833333f;
    public static float midColWidth = 0.9166667f;
    public static float playgroundWidth = 5.625f;
    public static bool IsOutOfScreen(BodySizeInfo info, Transform transform, Side side)
    {
        Vector2[] data = GetAllEdgeOfRect(info.centerPos + (Vector2)transform.position, info.bodySize * transform.localScale.x);
        foreach(var vector in data)
        {
            if (Mathf.Abs(vector.y) < maxPosY + offSet)
            {
                switch (side)
                {
                    case Side.Left:
                        if(vector.x >=-maxPosX && vector.x <= -midColWidth/ 2)
                        {
                            return false;
                        }
                        break;

                    case Side.Right:
                        if (vector.x <= maxPosX && vector.x >= midColWidth / 2)
                        {
                            return false;
                        }
                        break;
                }
            }         
        }
        return true;
    }
    public static bool IsOutOfScreen(BodySizeInfo info, Transform transform)
    {
        Vector2[] data = GetAllEdgeOfRect(info.centerPos + (Vector2)transform.position, info.bodySize * transform.localScale.x);
        foreach (var vector in data)
        {
            if (Mathf.Abs(vector.y) < maxPosY + offSet)
            {
                if (vector.x >= -maxPosX - offSet && vector.x <= maxPosX + offSet)
                {
                    return false;
                }
                break;
            }
        }
        return true;
    }

    /// <summary>
    /// Lấy trung tâm và độ dài 2 cạnh để trả về 4 đỉnh của 1 hình chữ nhật.
    /// 2 cặp đầu là chéo từ dưới lên, 2 cặp sau là chéo từ trên xuống.
    /// Tất cả theo giá trị của x tăng dần.
    /// </summary>
    /// <param name="center"></param>
    /// <param name="widthHeight"></param>
    /// <returns></returns>
    public static Vector2[] GetAllEdgeOfRect(Vector2 center,Vector2 widthHeight)
    {
        Vector2[] returnData = new Vector2[4]; 
        returnData[0] = center - widthHeight / 2; 
        returnData[1] = center + widthHeight / 2; 
        returnData[2] = center + new Vector2(-widthHeight.x/2, widthHeight.y/2);
        returnData[3] = center + new Vector2(widthHeight.x/2, -widthHeight.y);
        return returnData;
    }
    /// <summary>
    /// Trả về vị trí và kích thước của các cột trong game theo vị trí.
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static Vector2[] CollumnBodySize(Position pos)
    {
        Vector2[] returnData = new Vector2[2];
        Vector2 size, center;
        float height, width, posX, posY;

        switch (pos)
        {
            case Position.Left:
                posX = -Mathf.Abs(maxPosX - sideColWidth/2);
                posY = 0;
                width = sideColWidth;
                height = maxPosY * 2;
                break;
            case Position.Mid:
                posX = 0;
                posY = 0;
                width = midColWidth;
                height = maxPosY * 2;
                break;
            case Position.Right:
                posX = Mathf.Abs(maxPosX - sideColWidth / 2);
                posY = 0;
                width = sideColWidth;
                height = maxPosY * 2;
                break;
            default:
                height = 0;
                width = 0;
                posX = 0;
                posY = 0;
                break;
        }
        size = new Vector2(width,height);
        center = new Vector2(posX, posY);
        returnData[0] = center;
        returnData[1] = size;
        return returnData;
    }
    /// <summary>
    /// Kết quả trả về là theo thứ tự center sau đó là độ dài 2 cạnh
    /// </summary>
    /// <param name="side"></param>
    /// <returns></returns>
    public static Vector2[] PlayGround(Side side)
    {
        Vector2[] returnData = new Vector2[2];
        Vector2 middlePoint= new Vector2(-10,0);
        Vector2 offset = new Vector2(playgroundWidth, maxPosY * 2);
        switch (side)
        {
            case Side.Left:
                middlePoint.x = -(midColWidth / 2 + playgroundWidth / 2);
                break;
            case Side.Right:
                middlePoint.x = midColWidth / 2 + playgroundWidth / 2;
                break;
        }
        returnData[0] = middlePoint;
        returnData[1] = offset;
        return returnData;
    }

   
    #endregion

    #region SortingLayer and PlayGround
    /// <summary>
    /// Left side trả về từ 1 tới 5 và rigth trả về từ 6 - 10
    /// </summary>
    /// <param name="side"></param>
    /// <param name="layer"></param>
    /// <returns></returns>
    public static int SortingLayerFor(Side side, EntityLayer layer)
    {
        int sortingLayer = -10;
        switch (side)
        {
            case Side.Left:
                sortingLayer = SortingLayerForLeft(layer);
                break;
            case Side.Right:
                sortingLayer = SortingLayerForRight(layer);
                break;
        }
        return sortingLayer;
    }

    static int SortingLayerForLeft(EntityLayer layer)
    {
        int sortingLayer = -10;
        switch (layer)
        {
            case EntityLayer.Player:
                sortingLayer = 5;
                break;
            case EntityLayer.Bullet:
                sortingLayer = 4;
                break;
            case EntityLayer.Attack:
                sortingLayer = 3;
                break;
            case EntityLayer.Creep:
                sortingLayer = 2;
                break;
            case EntityLayer.Explosion:
                sortingLayer = 1;
                break;
        }
        return sortingLayer;
    }
    static int SortingLayerForRight(EntityLayer layer)
    {
        int sortingLayer = -10;
        switch (layer)
        {
            case EntityLayer.Player:
                sortingLayer = 10;
                break;
            case EntityLayer.Bullet:
                sortingLayer = 9;
                break;
            case EntityLayer.Attack:
                sortingLayer = 8;
                break;
            case EntityLayer.Creep:
                sortingLayer = 7;
                break;
            case EntityLayer.Explosion:
                sortingLayer = 6;
                break;
        }
        return sortingLayer;
    }
    
    public static Vector3 OffsetPositionForPlayground(Side side)
    {
        Vector3 offSet = Vector3.zero;
        switch (side)
        {
            case Side.Left:
                offSet.x = -maxPosX / 2;
                break; 
            case Side.Right:
                offSet.x = maxPosX / 2;
                break;
        }
        return offSet;
    }
    #endregion

    #region CreepSize
    public static Dictionary<Size, Vector3> ScaleDic = new Dictionary<Size, Vector3>()
    {
        {Size.One,Vector3.one * 0.5f },
        {Size.Two,Vector3.one * 0.75f },
        {Size.Three,Vector3.one * 1f },
        {Size.Four,Vector3.one * 1.25f },
        {Size.Five,Vector3.one * 1.5f }
    };
    public static Dictionary<Size, float> HitPointDic = new Dictionary<Size, float>()
    {
        {Size.One, 20 },
        {Size.Two, 30 },
        {Size.Three, 40 },
        {Size.Four, 50 },
        {Size.Five, 60 }
    };
    #endregion
    #region Attack Setting
    public static Side GetOpponentSide(Side mySide)
    {
        if (mySide == Side.Left)
        {
            return Side.Right;
        }
        return Side.Left;

    }
    public static Size GetForwardSide(Size current)
    {
        if(current == Size.Five)
        {
            return Size.Five;
        }
        return current + 1;
    }
    #endregion 
}
