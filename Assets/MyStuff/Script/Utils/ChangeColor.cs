﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public SpriteRenderer mySprite;
    public bool changeEffectOnly;
    public Color changeColor;
    public float intervalTime = 1f;
    float _time;
    Shader shaderGUIText;
    Shader shaderSpriteDefault;
    Color baseColor = Color.white;
    bool isChanging = false;
    void Start()
    {
        shaderGUIText = Shader.Find("GUI/Text Shader");
        shaderSpriteDefault = Shader.Find("Sprites/Default");
    }
    private void Update()
    {
        if (!gameObject.activeInHierarchy) return;
        _time += Time.deltaTime;
        if (_time < intervalTime) return;
        _time = 0;
        ChangeTheColor();
    }
    void ChangeTheColor()
    {
        if (changeEffectOnly)
        {
            if (!isChanging)
            {
                mySprite.color = changeColor;
                isChanging = true;
            }
            else
            {
                mySprite.color = baseColor;
                isChanging = false;
            }
            return;
        }

        if (!isChanging)
        {
            ChangeColorSprite();
            isChanging = true;
        }
        else
        {
            NormalColorSprite();
            isChanging = false;
        }
    }

    // Update is called once per frame
    void ChangeColorSprite()
    {
        mySprite.material.shader = shaderGUIText;
        mySprite.color = changeColor;
    }
    void NormalColorSprite()
    {
        mySprite.material.shader = shaderSpriteDefault;
        mySprite.color = baseColor;
    }
}
