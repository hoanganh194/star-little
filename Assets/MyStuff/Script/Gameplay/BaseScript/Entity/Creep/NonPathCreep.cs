﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonPathCreep : Creep
{
    public Transform myHost;

    NonPathCreepData myData;
    System.Action<NonPathCreep> onFinish;
    float rotationAngle = 0;
    float startAngle = 0;
    float rotationRadius = 0;
    Vector3 startPos;
    #region Hàm hỗ trợ 
    void ResetData()
    {
        onFinish = null;

        rotationAngle = 0;
        startAngle = 0;

        isInstall = false;
        isRunning = false;
        isFinish = false;
        isDead = false;
        deSpawned = false;
        canDamage = false;

        SetActiveRootChild(false);
        gameObject.SetActive(false);
    }
    #endregion
    public void SetData(Side side, Transform _myHost , NonPathCreepData creepData, System.Action<NonPathCreep> _onFinish)
    {
        ResetData();
        gameObject.SetActive(true);

        myHost = _myHost;
        myData = creepData;
        onFinish = _onFinish;
        base.SetData(side, myData.type);

        transform.position = myHost.position + myData.distanceOffset;
        rotationRadius = myData.distanceOffset.magnitude;
        startAngle = Mathf.Atan2(myData.distanceOffset.x, myData.distanceOffset.y) * Mathf.Rad2Deg;

        isInstall = true;
    }
    public void Spawn()
    {
        if (!isInstall) return;
        isRunning = true;
        canDamage = true;
    }
    protected virtual void FixedUpdate()
    {
        if (isInstall && isRunning)
        {
            MoveWithHost();
            rotationAngle += Time.deltaTime * myData.deltaTheta;
        }
    }
    void MoveWithHost()
    { 
        startPos = myHost.position + myData.distanceOffset;
        float newPosX;
        float newPosY;
        switch (myData.behaviour)
        {
            case SubMobBehaviour.Fix:
                transform.position = startPos;
                break;
            case SubMobBehaviour.RotateCounterClock:
                newPosX = rotationRadius * Mathf.Cos((rotationAngle + startAngle) * Mathf.Deg2Rad);
                newPosY = rotationRadius * Mathf.Sin((rotationAngle + startAngle) * Mathf.Deg2Rad);
                transform.position = myHost.position + new Vector3(newPosX, newPosY);
                break;
            case SubMobBehaviour.RotateNormal:
                newPosX = rotationRadius * Mathf.Cos((-rotationAngle + startAngle) * Mathf.Deg2Rad);
                newPosY = rotationRadius * Mathf.Sin((-rotationAngle + startAngle) * Mathf.Deg2Rad);
                transform.position = myHost.position - new Vector3(newPosX, newPosY);
                break;
        }
    }
    protected override void OnDeathMySeft()
    {
        if (deSpawned) return;
        isRunning = false;
        isFinish = true;
        canDamage = false;
        base.OnDeathMySeft();
    }
    protected override void SpawnAttack()
    {
        base.SpawnAttack();
        deSpawned = true;
        onFinish(this);
    }
}
