﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class PathCreep : Creep
{
    public DamageInfo damageInfo;
    PathCreator myPath;

    float distance;
    float speed;
    PathCreepData myData;
    NonPathCreepSO subMobData;
    Dictionary<NonPathCreep, bool> finishedCreepData;
    System.Action<PathCreep> onFinishAll;
    System.Action<PathCreep> onReadyAll;

    public List<StatusTest> testData = new List<StatusTest>();
    void TestUpdateFunction()
    {
        //Set if unity editor here;
        testData.Clear();
        foreach (var pair in finishedCreepData)
        {
            var newData = new StatusTest(pair.Key, pair.Value);
            testData.Add(newData);
        }
    }
    #region Hàm hỗ trợ 
    void ResetData()
    {
        if (finishedCreepData == null)
        {
            finishedCreepData = new Dictionary<NonPathCreep, bool>();
        }
        finishedCreepData.Clear();

        onFinishAll = null;
        onReadyAll = null;

        isInstall = false;
        isRunning = false;
        isFinish = false;
        isDead = false;
        deSpawned = false;
        canDamage = false;

        distance = 0;
        speed = 0;
        SetActiveRootChild(false);
        gameObject.SetActive(false);
    }
    #endregion
    #region Chuẩn bị dữ liệu
    public void SetData(Side side, PathCreator path, PathCreepData pathData, NonPathCreepSO subMobSO, 
        System.Action<PathCreep> _onReadyAll, System.Action<PathCreep> _onFinishAll)
    {
        ResetData();
        gameObject.SetActive(true);
        
        onFinishAll = _onFinishAll;
        onReadyAll = _onReadyAll;

        myPath = path;
        myData = pathData;
        subMobData = subMobSO;
        //Lấy dữ liệu về damageInfo từ DataManager
        
        base.SetData(side, myData.type);

        transform.position = new Vector3(-20, -20, -20);
        speed = myData.speed;
        CreateSubCreep();
        
        isInstall = true;
    }
    void CreateSubCreep()
    {
        if (subMobData == null) return;
        foreach(var creepData in subMobData.data)
        {
            var newCreep = Pool.Instance.Spawn(Utils.poolNameNonPathCreep).GetComponent<NonPathCreep>();
            newCreep.SetData(mySide, transform, creepData, OnFinishSubCreep);
            finishedCreepData.Add(newCreep, false);
        }
        onReadyAll(this);
    }
    #endregion
    #region Xuất phát
    public void Spawn()
    {
        if (!isInstall) return;
        isRunning = true;
        canDamage = true;
        foreach (var pair in finishedCreepData)
        {
            pair.Key.Spawn();
        }
    }
    protected virtual void FixedUpdate()
    {
        if(isInstall && isRunning)
        {
            TestUpdateFunction();
            distance += speed * Time.deltaTime;
            transform.position = myPath.path.GetPointAtDistance(distance, EndOfPathInstruction.Stop) + Utils.OffsetPositionForPlayground(mySide);
            OnEndPath();
        }
    }
    protected override void OnDeathMySeft()
    {
        isFinish = true;
        base.OnDeathMySeft();
  
    }
    protected override void SpawnAttack()
    {
        base.SpawnAttack();
        SetUpFinish();
    }
    void OnFinishSubCreep(NonPathCreep subCreep)
    {
        finishedCreepData[subCreep] = true;
        SetUpFinish();
    }
    #endregion
    #region Kết thúc path
    void OnEndPath()
    {
        float time = distance / myPath.path.length;
        if (time >= 1)
        {
            isFinish = true;
            isRunning = false;
            SetUpFinish(true);
        }
    }
    void SetUpFinish(bool force = false)
    {
        if (deSpawned) return;
        if (!isFinish) return;
        if (!force)
        {
            foreach (var pair in finishedCreepData)
            {
                if (pair.Value == false)
                {
                    return;
                }
            }
        }
        deSpawned = true;
        foreach (var pair in finishedCreepData)
        {
            Pool.Instance.Despawn(Utils.poolNameNonPathCreep, pair.Key.transform);
        }
        onFinishAll.Invoke(this);
    }
    #endregion
}