﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class Creep : Mob
{
    //Set size và thay sprite thêm armor cho các con creep theo hàm set data;
    public Size mySize;
    public bool isFinish = false;
    public bool deSpawned = false;
    public Vector3 myPrevPos;
    protected void SetData(Side side, Size size)
    {
        myLayer = EntityLayer.Creep;
        mySize = size;
        base.SetData(side, OnDeathMySeft);
        myBody.SetDataForBody(this, SpawnAttack);
        CreateMySeft();
    }
    void CreateMySeft()
    {
        //Get image from gameplayMnager
        if (Utils.ScaleDic.ContainsKey(mySize))
        {
            transform.localScale = Utils.ScaleDic[mySize];
            SetActiveRootChild(true);
            myCollider.enabled = true;
            currentHP = Utils.HitPointDic[mySize] / 2;
        }
        else if(mySize == Size.None)
        {
            SetActiveRootChild(false);
            myCollider.enabled = false;
            isFinish = true;
        }
    }
    protected virtual void OnDeathMySeft()
    {
        canDamage = false;
        myBody.SetDeath();
    }
    protected virtual void SpawnAttack()
    {
        SetActiveRootChild(false);
        myPrevPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        transform.position = new Vector3(-20, -20, -20);
        var explose = Pool.Instance.Spawn(Utils.poolNameExplosion).GetComponent<Explosion>();
        explose.SetData(this, mySide);
        explose.SetPositionAndSpawn(myPrevPos);
        EventsManager.Instance.OnCreepDeath(this);
    }
}
