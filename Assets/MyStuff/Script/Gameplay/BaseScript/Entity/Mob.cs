﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class này chứa thông tin về mob data: và gán vào mob như: hp, speed, damage (được set từ các class controller)
///  track collider riêng cho nhóm mob, thông tin explosion, 
/// </summary>
public abstract class Mob : Charactor
{
    public DamageInfo myDamageInfo;
    public List<EntityLayer> damageLayers = new List<EntityLayer>();
    protected bool canDamage = false;
    protected void SetData(Side _side, System.Action action)
    {
        damageLayers.Clear();
        mySide = _side;
        myDamageInfo.damagerPreference = this;
        switch (myLayer)
        {
            case EntityLayer.Creep:
                damageLayers.Add(EntityLayer.Player);
                myDamageInfo.value = 0.5f;
              
                break;
            case EntityLayer.Attack:
                damageLayers.Add(EntityLayer.Player);
                myDamageInfo.value = 3;
                break;
            default:
                Debug.LogError("Hiện tại chỉ có thể cho Creep và attack có thể check colldier với các entity khác");
                break;
        }
        base.SetData(action);
    }
    protected virtual void OnCollisionStay2D(Collision2D collision)
    {
        
        if (!isRunning || !isInstall || !canDamage) return;
        var controller = collision.gameObject.GetComponent<IDamageToChar>();
        if (controller == null) return;
        if (!controller.IsAlive) return;

      
        var layer= controller.Layer;
        var side= controller.Side;
        if ( mySide==side && damageLayers.Contains(layer))
        {
            controller.TakeDamage(myDamageInfo);
        }
    }
   
}
