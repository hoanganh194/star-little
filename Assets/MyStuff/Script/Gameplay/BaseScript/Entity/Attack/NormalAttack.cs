﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
/// <summary>
/// Thứ tự chính xác cho việc setdata:
/// nạp thông tin về vị trí và kích thước cho attack => tự động set kích thước, máu và collider
/// Nap path từ Manager
/// Cho khởi chạy 
/// </summary>
public class NormalAttack : BaseAttack
{
    public void SetData(Side _side, Size _size, float _speed, Vector2 startPos)
    {
        attackType = AttackType.Normal;
        poolName = Utils.poolNameNormalAttack;

        base.SetData(_side, _size, _speed);
        transform.position = startPos;
        if (GetPathFromManager())
        {
            isInstall = true;
            isRunning = true;
        }
    }

    bool GetPathFromManager()
    {
        var target = GameplayManager.Instance.playerManager.GetPlayer(mySide).transform.position + new Vector3(Random.Range(-0.5f, 0.5f), 0);
        myPath = AttackManager.Instance.GetNormalAttackPath(transform.position, target);
        if(myPath == null)
        {
            SeftDestruction();
            return false;
        }
        return true;
    }
    public void SetDataTest(Side _side, Size _size, VertexPath path, float _speed)
    {
        attackType = AttackType.Normal;
        poolName = Utils.poolNameNormalAttack;

        base.SetData(_side, _size, _speed);
        myPath = path;
        isInstall = true;
        isRunning = true;
    }
}
