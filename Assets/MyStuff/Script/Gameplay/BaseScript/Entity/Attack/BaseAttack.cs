﻿using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class BaseAttack : Attack
{
    protected float distance = 0;
    protected float speed = 5f;
    protected VertexPath myPath;
    public virtual void SetData(Side _side, Size _size, float _speed)
    {
        isRunning = false;
        distance = 0;
        speed = _speed;
        base.SetData(_side, _size);
    }
    protected virtual void Update()
    {
        if (!isInstall) return;
        if (!isRunning)  return;

        distance += speed * Time.deltaTime;
        transform.position = myPath.GetPointAtDistance(distance, EndOfPathInstruction.Stop);
        transform.eulerAngles = new Vector3(0, 0, rotationZCalculator(myPath.GetDirectionAtDistance(distance, EndOfPathInstruction.Stop)));

        if (distance / myPath.length >= 1 || IsOutOfScreen())
        {
            SeftDestruction();
        }
    }
}
