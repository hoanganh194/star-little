﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class ReverseAttack : BaseAttack
{
    public void SetData(Side _side, Size _size, float _speed, Vector2 startPos)
    {
        attackType = AttackType.Reverse;
        poolName = Utils.poolNameReverseAttack;

        base.SetData(_side, _size, _speed);
        transform.position = startPos;
        if (GetPathFromManager())
        {
            isInstall = true;
            isRunning = true;
        }
    }

    bool GetPathFromManager()
    {
        var target = GameplayManager.Instance.playerManager.GetPlayer(mySide).transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        myPath = AttackManager.Instance.GetNormalAttackPath(transform.position, target);
        if (myPath == null)
        {
            SeftDestruction();
            return false;
        }
        return true;
    }
    public void SetDataTest(Side _side, Size _size, VertexPath path, float _speed)
    {
        attackType = AttackType.Normal;
        poolName = Utils.poolNameReverseAttack;

        base.SetData(_side, _size, _speed);
        myPath = path;
        isInstall = true;
        isRunning = true;
    }
}
