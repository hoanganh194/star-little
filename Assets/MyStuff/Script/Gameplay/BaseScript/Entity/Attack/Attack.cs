﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attack : Mob
{
    public AttackType attackType;
    public Size mySize;
    public Vector3 myPrevPos;
    [SerializeField]
    protected string poolName="";
    protected void SetData(Side side, Size size)
    {
        myLayer = EntityLayer.Attack;
        mySize = size;
        base.SetData(side, OnDeathMySeft);
        CreateMySeft();
        canDamage = true;
    }
    void CreateMySeft()
    {
        //Get image from gameplayMnager
        if (Utils.ScaleDic.ContainsKey(mySize))
        {
            transform.localScale = Utils.ScaleDic[mySize];
            myBody.gameObject.SetActive(true);
            myCollider.enabled = true;
            currentHP = Utils.HitPointDic[mySize];
        }
        //Nếu không có size chứng tỏ là special or boss attack
    }
    protected float rotationZCalculator(Vector3 pos)
    {
        var angle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
        return angle + 90;
    }
    protected virtual void OnDeathMySeft()
    {
        myPrevPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        EventsManager.Instance.OnAttackDeath(this);
        SeftDestruction();
    }
    protected virtual void SeftDestruction()
    {
        canDamage = false;
        if (poolName != "")
        {
            Pool.Instance.Despawn(poolName, transform);
        }
    }
    protected virtual void ResetData()
    {
        mySize = Size.None;
        poolName = "";
    }
    protected override bool IsOutOfScreen()
    {
        return Utils.IsOutOfScreen(sizeInfo, transform);
    }
}
