﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class SpecialAttack : Attack
{
    protected VertexPath startPath;
    protected VertexPath endPath;
    protected float speed = 10;
    protected float distance = 0;
    public SpriteRenderer formingSprite;
    public virtual void SetData(Side _side, Vector2 startPos, string poolName)
    {
        transform.position = startPos;
        attackType = AttackType.Special;
        this.poolName = poolName;
        base.SetData(_side, Size.None);
        if (GetPathFromManager())
        {
            isInstall = true;
            isRunning = true;
            StartCoroutine(ActionFormingAttack());
        }
    }
    protected virtual bool GetPathFromManager()
    {
        var target = GameplayManager.Instance.playerManager.GetPlayer(mySide).transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        Vector3 midPoint = new Vector3(0, Mathf.Clamp(target.y - 1.5f, -Utils.maxPosY, Utils.maxPosY));
        startPath = AttackManager.Instance.GetNormalAttackPath(transform.position, midPoint, 4);
        endPath = AttackManager.Instance.GetNormalAttackPath(midPoint, target, 5);
        if (startPath == null || endPath == null)
        {
            SeftDestruction();
            return false;
        }
        return true;
    }
    protected virtual IEnumerator ActionFormingAttack()
    {
        yield return null;
        StartCoroutine(ActionFollowStartPath());
    }
    protected virtual IEnumerator ActionFollowStartPath()
    {

        distance = 0;
        while(distance / startPath.length <= 1)
        {
            distance += speed * Time.deltaTime;
            myRigidbody.transform.position = startPath.GetPointAtDistance(distance, EndOfPathInstruction.Stop);
            yield return Yielders.FixedUpdate;
        }
        StartCoroutine(ActionFollowEndPath());
    }
    protected virtual IEnumerator ActionFollowEndPath()
    {

        distance = 0;
        SetActiveRootChild(true);
        while (distance / endPath.length <= 1)
        {
            distance += speed * Time.deltaTime;
            myRigidbody.transform.position = endPath.GetPointAtDistance(distance, EndOfPathInstruction.Stop);
            yield return Yielders.FixedUpdate;
        }
        SeftDestruction();
    }
}
