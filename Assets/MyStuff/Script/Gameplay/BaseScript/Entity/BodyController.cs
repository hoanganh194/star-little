﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BodyController : MonoBehaviour
{
    public Animator animator;
    public SpriteRenderer spriteRendered;
    protected int mHashParaDeath = Animator.StringToHash("Death");
    protected int mHashParaAlive = Animator.StringToHash("Alive");
    protected int mHashParaHit = Animator.StringToHash("Hit");
    protected Entity host;
    System.Action onCompleted;
    public void SetDataForBody(Entity _host, System.Action onComplete)
    {
        onCompleted = onComplete;
        host = _host;
    }
    public void SetDeath()
    {
        animator.SetTrigger(mHashParaDeath);
    }    
    public void SetAlive()
    {
        animator.SetTrigger(mHashParaAlive);
    }
    public void ActionOnDeath()
    {
        onCompleted?.Invoke();
    }
    public void SetHit()
    {
        animator.SetTrigger(mHashParaHit);
    }
}
