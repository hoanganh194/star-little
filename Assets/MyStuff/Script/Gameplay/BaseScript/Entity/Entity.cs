﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
/// <summary>
/// Class này dùng để chứa thông tin về size, cameraSize, bodysize, và các hàm set collider tự động.
/// </summary>
public abstract class Entity : MonoBehaviour
{
	public BodySizeInfo sizeInfo;
	public Collider2D myCollider;
	public Rigidbody2D myRigidbody;
	public BodyController myBody;
	[HideInInspector] public SortingGroup sortingGroup;

	[HideInInspector]
	[SerializeField] protected Side mySide = Side.None;
	[HideInInspector]
	[SerializeField] protected EntityLayer myLayer = EntityLayer.None;

	[HideInInspector]
	[SerializeField] protected bool isInstall = false;
	[HideInInspector]
	[SerializeField] protected bool isRunning = false;
	public Side Side { get { return mySide; } }
	public EntityLayer Layer { get { return myLayer; } }
	public bool IsInstall { get { return isInstall; } }
	public bool IsRunning { get { return isRunning; } }


	protected virtual void CalculateCollider()
    {
		myCollider = GetComponent<BoxCollider2D>();
		myRigidbody = GetComponent<Rigidbody2D>();
		sortingGroup = GetComponent<SortingGroup>();
		if (!myCollider)
		{
			myCollider = gameObject.AddComponent<BoxCollider2D>();
			var collider = (BoxCollider2D)myCollider;
			collider.offset = sizeInfo.centerPos;
			collider.size = sizeInfo.bodySize;
		}
		if (!myRigidbody)
		{
			myRigidbody = gameObject.AddComponent<Rigidbody2D>();
		}
		if (!sortingGroup)
		{
			sortingGroup = gameObject.AddComponent<SortingGroup>();
		}
		myRigidbody.isKinematic = true;
		myRigidbody.useFullKinematicContacts = true;
		sizeInfo.sizeOnCamera *= transform.localScale.x;
		sortingGroup.sortingOrder = Utils.SortingLayerFor(mySide, myLayer);
	}
	protected virtual bool IsOutOfScreen()
    {
		return Utils.IsOutOfScreen(sizeInfo, transform, mySide);
	}
	public virtual void SetActiveRootChild(bool active)
    {
		myBody.gameObject.SetActive(active);
    }
    public virtual void RemoveAllCallback() { }
	void OnDrawGizmos()
	{
        // Draw a yellow sphere at the transform's position
        if (mySide == Side.Left)
        {
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireCube(transform.position + (Vector3)sizeInfo.centerPos, (Vector3)sizeInfo.bodySize);
		}
		else
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireCube(transform.position + (Vector3)sizeInfo.centerPos, (Vector3)sizeInfo.bodySize);
		}
	}
	public Vector3 sizeEffectDependOnBodySize
	{
		get
		{
			if (_sizeEffectDependOnBodySize.Equals(Vector3.zero))
			{
				_sizeEffectDependOnBodySize = (Vector3)sizeInfo.bodySize;
				float _maxSize = _sizeEffectDependOnBodySize.x;
				if (_sizeEffectDependOnBodySize.x < _sizeEffectDependOnBodySize.y)
				{
					_maxSize = _sizeEffectDependOnBodySize.y;
				}
				_sizeEffectDependOnBodySize.x = _maxSize;
				_sizeEffectDependOnBodySize.y = _maxSize;
				_sizeEffectDependOnBodySize.z = _maxSize;
			}
			return _sizeEffectDependOnBodySize;
		}
	}
	protected Vector3 _sizeEffectDependOnBodySize = Vector3.zero;

}
