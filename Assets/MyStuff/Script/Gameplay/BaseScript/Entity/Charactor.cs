﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class này chứa thông tin về thực thể có thể nhận damage
/// Thông tin gồm khả năng nhận damage, chết làm gì, coi còn sống không
/// </summary>
public abstract class Charactor : Entity, IDamageToChar
{
	public float currentHP = 5f;
    public bool IsAlive { get; private set; } = true;
    System.Action myDeathAction;
	public bool isDead = false;
	protected void SetData(System.Action action)
	{
		myDeathAction = action;
		IsAlive = true;
		base.CalculateCollider();
	}
	public virtual void TakeDamage(DamageInfo damageInfo)
	{
		if (currentHP < 1 && damageInfo.value < 1)
        {
			return;
        }
		//Debug.Log($"{gameObject.name}: {mySide} take {damageInfo.value} damge by {damageInfo.damagerPreference.gameObject.name}");
		currentHP -= damageInfo.value;
		CheckForHitPoint();
	}
	
    protected virtual void CheckForHitPoint()
    {
		if (currentHP <= 0)
		{
			IsAlive = false;
			OnKill();
		}
	}
	protected virtual void OnKill()
    {
		if (isDead) return;
		isDead = true;
        if (myDeathAction != null)
        {
			myDeathAction.Invoke();
        }
    }
	public virtual bool CanBeDamage()
    {
		return !IsOutOfScreen();
    }
}
