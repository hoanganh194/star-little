﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntity
{
    bool IsInstall { get; }
    EntityLayer Layer { get; }
    Side Side { get; }
    void SetActiveRootChild(bool active);
    bool IsRunning { get; }
}
public interface IPlayer
{
    void Move(Vector3 direction);
    void ShootNormal();
    void ShootCharge(bool press);
    void UseSkill();

}
public interface IDamageToChar
{
    bool IsAlive { get; }
    void TakeDamage(DamageInfo damInfo);
    EntityLayer Layer { get; }
    Side Side { get; }
    bool CanBeDamage();
}
public interface IBullet
{
    BulletType GetBulletType();
    bool IsRunning { get; }
    void SetPositionAndSpawn(Vector3 position, Vector3 direction);
}
public interface IExplosion
{
   
}


