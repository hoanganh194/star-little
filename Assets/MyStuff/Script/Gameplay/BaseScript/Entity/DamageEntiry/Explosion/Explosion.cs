﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : DamageEntity ,IExplosion
{
    public ExplosionInfo info;
    public Animator anim;
    protected float _currentTime = 0f;
    protected int _mHashParaExplose = Animator.StringToHash("Explose");
    /// <summary>
    /// Sau này cần tuỳ chỉnh để coi ai tạo ra vụ nổ. Hiện tại chỉ chấp nhận vụ nổ từ creep và player
    /// </summary>
    public virtual void SetData(Creep whoCreate ,Side _side = Side.None)
    {
        myLayer = EntityLayer.Explosion;
        damageInfo = info.myDamage;
        damageInfo.damagerPreference = this;

        damageLayers.Add(EntityLayer.Creep);
        damageLayers.Add(EntityLayer.Attack);
        base.SetData(_side);
        isInstall = true;
        myBody.SetDataForBody(this, DespawnEarly);
    }
    public virtual void SetPositionAndSpawn(Vector3 position)
    {
        transform.position = position;
        StartCoroutine(UpdateTimmer());
    }

    protected virtual IEnumerator UpdateTimmer()
    {
        _currentTime = 0;
        while(_currentTime < info.delayTime)
        {
            _currentTime += 0.1f;
            yield return Yielders.Get(0.1f);
        }
        //anim.PlayInFixedTime(_mHashParaExplose, -1, info.duration);
        SetActiveRootChild(true);
        isRunning = true;
        base.Spawn();
        while (_currentTime < info.delayTime + info.duration)
        {
            _currentTime += 0.1f;
            yield return Yielders.Get(0.1f);
        }
        Despawn();
    }
    protected override void Despawn()
    {
        isRunning = false;
        base.Despawn();
        Pool.Instance.Despawn(Utils.poolNameExplosion, transform);
    }
    public void DespawnEarly()
    {
        Despawn();
    }
}
