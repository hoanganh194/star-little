﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class DamageEntity : Entity
{

    [HideInInspector] [SerializeField]
    protected DamageInfo damageInfo;
    [HideInInspector] [SerializeField]
    protected List<EntityLayer> damageLayers = new List<EntityLayer>();
    [HideInInspector] [SerializeField]
    protected List<IDamageToChar> listDamaged = new List<IDamageToChar>();
    protected void SetData(Side side)
    {
        listDamaged.Clear();
        mySide = side;
        CalculateCollider();
        myCollider.enabled = false;
    }
    protected virtual void Spawn()
    {
        myCollider.enabled = true;
    }
    protected virtual void OnCollisionStay2D(Collision2D collision)
    {
        if (!isRunning || !isInstall) return;
        var controller = collision.gameObject.GetComponent<IDamageToChar>();
        if (controller == null || !controller.IsAlive || !controller.CanBeDamage()) return;
        if (listDamaged.Contains(controller)) return;
        
        var layer = controller.Layer;
        var side = controller.Side;
        if (mySide == side && damageLayers.Contains(layer))
        {
            AddInfoToTrack(controller);
            OnHitTarget(controller);
        }
    }
    protected virtual void OnHitTarget(IDamageToChar controller)
    {
        controller.TakeDamage(damageInfo);
    }
    protected virtual void Despawn()
    {
        ResetTrackInfo();
        myCollider.enabled = false;
        isInstall = false;
    }
    #region Support
    protected void AddInfoToTrack(IDamageToChar damageOb)
    {
        if (!isInstall) return;
        listDamaged.Add(damageOb);
    }
    protected void ResetTrackInfo()
    {
        if (!isInstall) return;
        listDamaged.Clear();
    }

    #endregion
}
