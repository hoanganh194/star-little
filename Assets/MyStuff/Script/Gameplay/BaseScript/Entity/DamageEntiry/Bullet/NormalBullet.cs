﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Đạn này specify cho riêng player.
/// </summary>
public class NormalBullet : Bullet
{
    public NormalBulletInfo bulletInfo;
    [HideInInspector] public Player whoShootMe;
    public void SetData(Charactor _whoShootMe, string myPoolName)
    {
        whoShootMe = (Player)_whoShootMe;
        damageInfo = bulletInfo.myDamage;
        damageInfo.damagerPreference = whoShootMe;
        base.SetData(myPoolName, whoShootMe.Side);
        damageLayers.Add(EntityLayer.Creep);
        damageLayers.Add(EntityLayer.Attack);
        isInstall = true;
    }
    public override void SetPositionAndSpawn(Vector3 position, Vector3 direction)
    {
        speed = bulletInfo.speed;
        base.SetPositionAndSpawn(position, direction);
    }
    protected override void OnHitTarget(IDamageToChar controller)
    {
        base.OnHitTarget(controller);
        Despawn();
    }

}
