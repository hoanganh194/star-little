﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public abstract class Bullet : DamageEntity ,IBullet
{

    [HideInInspector] public float speed = 1;
    [HideInInspector] [SerializeField] protected BulletType myBulletType;
    [HideInInspector] [SerializeField] protected string myPoolName;
    [HideInInspector] [SerializeField] protected Vector3 direction = Vector3.up;

    protected IEnumerator moveAction;
    public virtual BulletType GetBulletType()
    {
        return myBulletType;
    }
    protected void SetData(string poolName, Side side)
    {
        myPoolName = poolName;
        myLayer = EntityLayer.Bullet;
        base.SetData(side);
    }
    public virtual void SetPositionAndSpawn(Vector3 position, Vector3 direction)
    {
        this.direction = direction;
        transform.position = position;
        isRunning = true;
        isInstall = true;
        SetActiveRootChild(true);
        moveAction = Move();
        StartCoroutine(moveAction);
        Spawn();
    }
    protected virtual IEnumerator Move()
    {
        if (!IsInstall) yield break;
        while (!IsOutOfScreen())
        {
            myRigidbody.transform.position += direction * speed * Time.deltaTime;
            yield return Yielders.FixedUpdate;
        }
        Despawn();
    }

    protected override void Despawn()
    {
        isRunning = false;
        base.Despawn();
        Pool.Instance.Despawn(myPoolName, transform);
    }

}