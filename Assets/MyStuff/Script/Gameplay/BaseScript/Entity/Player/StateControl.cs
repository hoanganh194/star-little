﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateControl : MonoBehaviour
{
    public Player player;
    public bool isActive = false;
    [SerializeField] PlayerInfo info;
    StateBehaviour stateNone;
    StateBehaviour stateNormal;
    StateBehaviour stateCharge;
    StateBehaviour stateSkill;
    StateBehaviour stateStun;
    StateBehaviour stateDeath;
    StateBehaviour stateHit;

    [SerializeField] public StateBehaviour activeState;
    [SerializeField] public StateBehaviour prevState;

    /// <summary>
    /// State controller sẽ set data.
    /// Sau này có thể thêm các setting cho stage tuỳ chỉnh tại đây.
    /// </summary>
    public void SetUpData(PlayerInfo data)
    {
        info = data;
        stateNone =     new StateBehaviour(PlayerStatus.None, false);
        stateNormal =   new StateBehaviour(PlayerStatus.Normal, false);

        stateCharge =   new StateBehaviour(PlayerStatus.Charge, false);
        stateSkill =    new StateBehaviour(PlayerStatus.Skill, true, info.skillInvulnerableTime);
        stateStun =     new StateBehaviour(PlayerStatus.Stun, true, info.stunTime);

        stateHit =      new StateBehaviour(PlayerStatus.Hit, true, info.hitInvulnerableTime);
        stateDeath =    new StateBehaviour(PlayerStatus.Death, false);

        stateNone.AddNextStates(stateNormal);
        stateNormal.AddNextStates(stateCharge, stateSkill, stateStun, stateHit,  stateDeath);

        stateCharge.AddNextStates(stateNormal, stateStun, stateHit, stateDeath);
        stateSkill.AddNextStates(stateNormal);
        
        stateStun.AddNextStates(stateNormal, stateHit, stateDeath);
        stateHit.AddNextStates(stateNormal);

        activeState = stateNormal;
        prevState = stateNone;
        isActive = true;
    }
    void Update()
    {
        if (!isActive) return;
        if (activeState!=null)
        {
            player.status = activeState.status;
            activeState.currentTime += Time.deltaTime;
            if (activeState.isConstant)
            {
                if (activeState.currentTime >= activeState.fixTime)
                {
                    if (activeState.status == PlayerStatus.Stun)
                    {
                        StartCoroutine(SetStateToWeak());
                    }
                    ChangeToNormalState();
                }
            }
        }
    }
    void ChangeToNormalState()
    {
        prevState = activeState;
        activeState = stateNormal;
        activeState.Reset();
    }
    public bool ChangeState(PlayerStatus stage)
    {
        foreach(var nextState in activeState.nextStages)
        {
            if(nextState.status == stage)
            {
                prevState = activeState;
                activeState = nextState;
                activeState.Reset();
                return true;
            }
        }
        return false;
    }
    IEnumerator SetStateToWeak()
    {
        player.shootingStatus = PlayerShootingStatus.Weak;
        yield return Yielders.Get(player.info.weakTime);
        player.shootingStatus = PlayerShootingStatus.Normal;
    }

}
public class StateBehaviour
{
    public PlayerStatus status;
    public float currentTime;
    public bool isConstant = false;
    public float fixTime;
    public List<StateBehaviour> nextStages;
    public StateBehaviour() { }
    public StateBehaviour(PlayerStatus newStatus,bool fix, float maxTime = 0) 
    {
        status = newStatus;
        currentTime = 0;
        isConstant = fix;
        fixTime = maxTime;
    }
    public void AddNextStates(params StateBehaviour[] list)
    {
        nextStages = new List<StateBehaviour>();
        foreach (var thing in list)
        {
            nextStages.Add(thing);
        }
    }
    public void Reset()
    {
        currentTime = 0;
    }
}
