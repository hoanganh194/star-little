﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
public abstract class Player : Charactor , IPlayer
{
    public PlayerInfo info;
    public NormalBullet normalBullet;
    public ChargeBullet chargeBullet;
    public SpecialAttack specialAttack;

    #region Internal Data
    [HideInInspector] public PlayerStatus status = PlayerStatus.None;
    [HideInInspector] public PlayerShootingStatus shootingStatus = PlayerShootingStatus.Normal;
    StateControl stateController;
    IEnumerator shootAction;
    Vector2 rangeX;
    Vector2 rangeY;
    float speed = 0;
    string normalBulletPool = "";
    string chargeBulletPool = "";
    PlayerController myController;
    #endregion
    #region SettingData
    public void SetData(Side side)
    {
        myLayer = EntityLayer.Player;
        mySide = side;
        CreateController();
        CreateBulletPool();
        CreateStatusController();
        PlayGroundCalculate();
        base.SetData(OnDeath);
        isInstall = true;
    }
    void CreateController()
    {
        myController = gameObject.AddComponent<PlayerController>();
        myController.SetData(mySide, this);
    }
    void PlayGroundCalculate()
    {
        var moveArea = Utils.PlayGround(mySide);
        var fourEdge = Utils.GetAllEdgeOfRect(moveArea[0], moveArea[1]);
        rangeX = new Vector2(fourEdge[0].x + sizeInfo.sizeOnCamera.x / 2, fourEdge[1].x - sizeInfo.sizeOnCamera.x / 2);
        rangeY = new Vector2(fourEdge[0].y + sizeInfo.sizeOnCamera.y / 2, fourEdge[1].y - sizeInfo.sizeOnCamera.y / 2);
        transform.position = moveArea[0] + Vector2.down * Utils.maxPosY / 2 ;
    }
    protected virtual void CreateStatusController()
    {
        stateController = gameObject.AddComponent<StateControl>();
        stateController.SetUpData(info);
        stateController.player = this;
    }
    protected void CreateBulletPool()
    {
        normalBulletPool = Utils.PoolName(BulletType.Normal, mySide);
        chargeBulletPool = Utils.PoolName(BulletType.Charge, mySide);

        var _normalBullet = Instantiate(normalBullet);
        _normalBullet.SetData(this, normalBulletPool);

        var _chargeBullet = Instantiate(chargeBullet);
        _chargeBullet.SetData(this, chargeBulletPool);

        Pool.Instance.CreatePool(normalBulletPool, _normalBullet.transform, 2);
        Pool.Instance.CreatePool(chargeBulletPool, _chargeBullet.transform, 2);

        Destroy(_normalBullet.gameObject);
        Destroy(_chargeBullet.gameObject);
    }
    void OnDeath()
    {
        Debug.Log($"Game over player {mySide} is lose");
    }
    #endregion
    #region Attack
    protected virtual void SetUpShoot()
    {
        if (status != PlayerStatus.Normal) return;
        if (shootAction == null)
        {
            shootAction = DoActionShot();
            StartCoroutine(shootAction);
        }
    }
    protected virtual IEnumerator DoActionShot()
    {
        var bulletTransform = Pool.Instance.Spawn(normalBulletPool);
        if (bulletTransform)
        {
            var bullet = bulletTransform.GetComponent<NormalBullet>();
            bullet.SetPositionAndSpawn(transform.position, Vector3.up);
        }
        yield return Yielders.Get(normalBullet.bulletInfo.attackInterval);
        shootAction = null;
    }
    protected virtual void SetUpChargeShoot(bool press)
    {
        if (press)
        {
            stateController.ChangeState(PlayerStatus.Charge);
        }
        else
        {
            if (status != PlayerStatus.Charge) return;
            stateController.ChangeState(PlayerStatus.Normal);
            if(stateController.prevState.currentTime >= chargeBullet.bulletInfo.chargeTime)
            {
                StartCoroutine(DoActionShotCharge());
            }
        }
    }
    protected virtual IEnumerator DoActionShotCharge()
    {
        var bullet = Pool.Instance.Spawn(chargeBulletPool).GetComponent<ChargeBullet>();
        if (bullet)
        {
            bullet.SetPositionAndSpawn(transform.position, Vector3.up);
        }
        yield break;
    }


    #endregion
    #region IPlayer
    public virtual void Move(Vector3 direction) 
    {
        if (!isInstall) return;
        speed = info.maxSpeed;
        if (shootingStatus == PlayerShootingStatus.Weak)
        {
            speed = info.maxSpeed * info.percentWeak;
        }
        if (status==PlayerStatus.Normal || status == PlayerStatus.Charge)
        {
            Vector3 delta = direction.normalized * speed * Time.deltaTime;
            Vector3 pos = transform.position;
            pos.x = Mathf.Clamp(pos.x + delta.x, rangeX.x, rangeX.y);
            pos.y = Mathf.Clamp(pos.y + delta.y, rangeY.x, rangeY.y);
            myRigidbody.transform.position = pos;
        }
    }
        
    public virtual void ShootNormal()
    {
        SetUpShoot();
    }
    public virtual void ShootCharge(bool press)
    {
        SetUpChargeShoot(press);
    }
    public virtual void UseSkill()
    {

    }

    #endregion
    #region IDamage
    public override void TakeDamage(DamageInfo damageInfo)
    {
        if (damageInfo.value >= 2)
        {
            if (stateController.ChangeState(PlayerStatus.Hit))
            {
                base.TakeDamage(damageInfo);
                myBody.SetHit();
            }
        }
        else
        {
            if (stateController.ChangeState(PlayerStatus.Stun))
            {
                base.TakeDamage(damageInfo);
                StartCoroutine(AddForceByCreep(damageInfo.damagerPreference.transform.position));
            }
        }
    }

    public virtual IEnumerator AddForceByCreep(Vector3 posOfEnemy)
    {
        Vector3 delta = (transform.position - posOfEnemy).normalized;
        float force = 1f;
        while(force >= 0)
        {
            Vector3 forceRaw = delta * force;
            Vector3 pos = transform.position + forceRaw;

            if( pos.x >= rangeX.y || pos.x <= rangeX.x)
            {
                forceRaw = new Vector3(-forceRaw.x, forceRaw.y);
            }
            if (pos.y >= rangeY.y || pos.y <= rangeY.x)
            {
                forceRaw = new Vector3(forceRaw.x, -forceRaw.y);
            }
            pos = transform.position + forceRaw;
            myRigidbody.transform.position = pos;
            yield return Yielders.Get(0.01f);
            force -= 0.2f;
        }
    }
   
    #endregion
}
