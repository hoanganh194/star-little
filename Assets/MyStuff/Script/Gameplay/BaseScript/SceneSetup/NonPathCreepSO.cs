﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New NonPathCreep Info", menuName = "Wave/NonPathCreep Info", order = 3)]
public class NonPathCreepSO : ScriptableObject
{
    public List<NonPathCreepData> data;
}
