﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathDisplay : MonoBehaviour
{
    public BodySizeInfo sizeInfo;
	void OnDrawGizmos()
	{
		// Draw a yellow sphere at the transform's position
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireCube((Vector3)sizeInfo.centerPos, (Vector3)sizeInfo.bodySize);

		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube((Vector3)sizeInfo.centerPos, (Vector3)sizeInfo.sizeOnCamera);
	}
}
