﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayGround : MonoBehaviour
{
    public BodySizeInfo sizeInfo;
	public Side myPosition;

	Vector2[] dataPosition;
    private void Awake()
    {
		dataPosition = Utils.PlayGround(myPosition);
		sizeInfo.centerPos = dataPosition[0];
		sizeInfo.bodySize = dataPosition[1];

		transform.position = sizeInfo.centerPos;
		transform.localScale = new Vector3(sizeInfo.bodySize.x, sizeInfo.bodySize.y);


	}
    void OnDrawGizmos()
	{
		// Draw a yellow sphere at the transform's position
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireCube((Vector3)sizeInfo.centerPos, (Vector3)sizeInfo.bodySize);

		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube((Vector3)sizeInfo.centerPos, (Vector3)sizeInfo.sizeOnCamera);
	}
}
