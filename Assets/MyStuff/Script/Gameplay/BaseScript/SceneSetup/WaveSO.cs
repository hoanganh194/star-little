﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Wave", menuName = "Wave/New Wave",order =1)]
public class WaveSO : ScriptableObject
{
    public EventWave eventWave = EventWave.None;
    public List<PatternInfo> listPatterns;
}
[System.Serializable]
public class PatternInfo
{
    [Header("Thời gian delay so với pattern đầu tiên nhỏ nhất là 0")]
    public float timeDelay;
    public PatternSO pattern;
}
