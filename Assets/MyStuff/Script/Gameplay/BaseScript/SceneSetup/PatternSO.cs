﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Pattern", menuName = "Wave/New Pattern", order = 2)]
public class PatternSO : ScriptableObject
{
    public PathCreation.PathCreator path;
    public SpeedOverride speedOverride;
    public List<PathCreepInfo> creepFollowThisPath;
}
[System.Serializable]
public class SpeedOverride
{
    [Header("Nếu set isOverride true thì")]
    public bool isOveride = false;
    [Header("tất cả mob sẽ di chuyển với vận tốc bên dưới")]
    public float speed = 5;
}
[System.Serializable]
public class PathCreepInfo
{
    [Header("Thời gian chờ trước khi bắt di chuyển")]
    public float waitTime = 0.2f;
    public PathCreepData pathCreepData;
    public NonPathCreepSO followNonPathCreep;
}