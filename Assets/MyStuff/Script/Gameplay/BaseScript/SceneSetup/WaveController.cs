﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class WaveController : MonoBehaviour
{
    public Side mySide = Side.Left;
    public WaveSO currentActiveWave;

    public List<StatusTest> testData = new List<StatusTest>();
    Dictionary<PatternController, StatusData> spawnedPatterns;
    IEnumerator fetchAction = null;
    public bool spawnedFirstWave = false;
    System.Action<WaveController> onReadyToSpawn;

    void TestUpdateFunction()
    {
        //Set if unity editor here;
        testData.Clear();
        foreach (var pair in spawnedPatterns)
        {
            var newData = new StatusTest(pair.Key, pair.Value.complete);
            testData.Add(newData);
        }
    }
    public void SetData(Side side, System.Action<WaveController> _onReady)
    {
        if(spawnedPatterns == null)
        {
            spawnedPatterns = new Dictionary<PatternController, StatusData>();
        }
        mySide = side;
        spawnedFirstWave = false;
        onReadyToSpawn = _onReady;
    }
    public void FetchWaveAndSetData()
    {
        if (fetchAction == null)
        {
            fetchAction = FetchWaveAndInstallPatternController();
            StartCoroutine(fetchAction);
        }
    }
    IEnumerator FetchWaveAndInstallPatternController()
    {
        yield return Yielders.Get(0.5f);
        spawnedPatterns.Clear();
        currentActiveWave = WaveManager.Instance.FetchWave(mySide);
        foreach (var pattern in currentActiveWave.listPatterns)
        {
            var newController = Pool.Instance.Spawn(Utils.poolNamePattern).GetComponent<PatternController>();
            var status = new StatusData(pattern.timeDelay);
            spawnedPatterns.Add(newController, status);
        }
        int index = 0;
        foreach(var pair in spawnedPatterns)
        {
            pair.Key.SetData(mySide, currentActiveWave.listPatterns[index].pattern, OnFinishPattern, OnFinishSetDataPattern);
            index++;
        }
        TestUpdateFunction();
        fetchAction = null;
        yield break;
    }
    void OnFinishSetDataPattern(PatternController pattern)
    {
        spawnedPatterns[pattern].isInstall = true;
        foreach (var pair in spawnedPatterns)
        {
            if (pair.Value.isInstall == false)
            {
                return;
            }
        }
        ActionWhenReadyToSpawn();
    }
    void ActionWhenReadyToSpawn()
    {
        if (!spawnedFirstWave)
        {
            onReadyToSpawn?.Invoke(this);
            return;
        }
    }
    public void ActionSpawnWave()
    {
        StartCoroutine(SpawnPatterns());
    }
    IEnumerator SpawnPatterns()
    {
        while (!CheckAllStarted())
        {
            foreach (var pair in spawnedPatterns)
            {
                if (pair.Value.delayTime > 0)
                {
                    pair.Value.delayTime -= 0.02f;

                }
                else if (!pair.Value.started)
                {
                    pair.Value.started = true;
                    pair.Key.gameObject.SetActive(true);
                    pair.Key.Spawn();
                }
            }
            yield return new WaitForSeconds(0.02f);
        }
        
    }
    void OnFinishThisWave()
    {
        fetchAction = FetchWaveAndInstallPatternController();
        StartCoroutine(fetchAction);
    }
    void OnFinishPattern(PatternController pattern)
    {
        spawnedPatterns[pattern].complete = true;
        TestUpdateFunction();
        if (CheckAllCompleted())
        {
            foreach (var pair in spawnedPatterns)
            {
                Pool.Instance.Despawn(Utils.poolNamePattern, pair.Key.transform);
            }
            spawnedPatterns.Clear();
            OnFinishThisWave();
        }
    }
    bool CheckAllStarted()
    {
        foreach (var pair in spawnedPatterns)
        {
            if (pair.Value.started == false)
            {
                return false;
            }
        }
        return true;
    }
    bool CheckAllCompleted()
    {
        foreach (var pair in spawnedPatterns)
        {
            if (pair.Value.complete == false)
            {
                return false;
            }
        }
        return true;
    }

}
