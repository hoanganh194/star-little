﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PatternController : MonoBehaviour
{
    public Side mySide;
    public PatternSO myPattern;
    Dictionary<PathCreep, StatusData> spawnedPathCreep;
    System.Action<PatternController> onFinish;
    System.Action<PatternController> onReady;
    public List<StatusTest> testData = new List<StatusTest>();
    //PathCreator instancePath;
    void TestUpdateFunction()
    {
        //Set if unity editor here;
        testData.Clear();
        foreach (var pair in spawnedPathCreep)
        {
            var newData = new StatusTest(pair.Key, pair.Value.complete);
            testData.Add(newData);
        }
    }
    public void SetData(Side side, PatternSO pattern, System.Action<PatternController> _onFinish, System.Action<PatternController> _onReady)
    {
        if (spawnedPathCreep == null)
        {
            spawnedPathCreep = new Dictionary<PathCreep, StatusData>();
        }
        spawnedPathCreep.Clear();

        mySide = side;
        myPattern = pattern;
        onFinish = _onFinish;
        onReady = _onReady;

        CreatePathCreep();
    }
    void CreatePathCreep()
    {
        foreach (var creepData in myPattern.creepFollowThisPath)
        {
            if (myPattern.speedOverride.isOveride)
            {
                creepData.pathCreepData.speed = myPattern.speedOverride.speed;
            }

            var newCreepLeader = Pool.Instance.Spawn(Utils.poolNamePathCreep).GetComponent<PathCreep>();
            spawnedPathCreep.Add(newCreepLeader, new StatusData(creepData.waitTime));
        }
        int index = 0;
        foreach (var pair in spawnedPathCreep)
        {
            pair.Key.SetData(mySide, myPattern.path, myPattern.creepFollowThisPath[index].pathCreepData, myPattern.creepFollowThisPath[index].followNonPathCreep, 
                OnFinishCreatePathCreep, OnFinishPathCreep);
            index++;
        }
        TestUpdateFunction();
    }
    void OnFinishCreatePathCreep(PathCreep pathCreep)
    {
        if (!spawnedPathCreep.ContainsKey(pathCreep))
        {
            Debug.LogError("Không tìm thấy pathCreep");
            return;
        }
        spawnedPathCreep[pathCreep].isInstall = true;
        foreach (var pair in spawnedPathCreep)
        {
            if (pair.Value.isInstall == false)
            {
                return;
            }
        }
        onReady(this);
    }
    public void Spawn()
    {
        StartCoroutine(StartToSpawn());
    }
    IEnumerator StartToSpawn()
    {
        while (!CheckAllStarted())
        {
            foreach (var pair in spawnedPathCreep)
            {
                if (pair.Value.delayTime > 0)
                {
                    pair.Value.delayTime -= 0.02f;

                }
                else if (!pair.Value.started)
                {
                    pair.Value.started = true;
                    pair.Key.gameObject.SetActive(true);
                    pair.Key.Spawn();
                }
            }
            yield return new WaitForSeconds(0.02f);
        }
    }
    void OnFinishPathCreep(PathCreep pathCreep)
    {
        spawnedPathCreep[pathCreep].complete = true;
        TestUpdateFunction();
        if (CheckAllCompleted())
        {
            foreach(var pair in spawnedPathCreep)
            {
                Pool.Instance.Despawn(Utils.poolNamePathCreep, pair.Key.transform);
            }
            spawnedPathCreep.Clear();
            onFinish.Invoke(this);
        }
    }
    bool CheckAllStarted()
    {
        foreach (var pair in spawnedPathCreep)
        {
            if (pair.Value.started == false)
            {
                return false;
            }
        }
        return true;
    }
    bool CheckAllCompleted()
    {
        foreach (var pair in spawnedPathCreep)
        {
            if (pair.Value.complete == false)
            {
                return false;
            }
        }
        return true;
    }
}

