﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BackgroundCol : MonoBehaviour
{
    public Position myPosition;
    public BodySizeInfo sizeInfo;
    private void Awake()
    {
		var dataPosition = Utils.CollumnBodySize(myPosition);
		sizeInfo.centerPos = dataPosition[0];
		sizeInfo.bodySize = dataPosition[1];

		transform.position = sizeInfo.centerPos;
		transform.localScale = new Vector3(sizeInfo.bodySize.x, sizeInfo.bodySize.y);
	}
    void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube((Vector3)sizeInfo.centerPos, (Vector3)sizeInfo.bodySize);
	}

}
