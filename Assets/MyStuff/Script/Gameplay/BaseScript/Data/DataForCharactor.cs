﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class BodySizeInfo
{
    public Vector2 sizeOnCamera = Vector2.one; 
    public Vector2 bodySize = Vector2.one;
    public Vector2 centerPos = Vector2.zero; 
}

[Serializable]
public class PlayerInfo
{
    public float maxHp = 5f;
    public float maxSpeed = 5f;
    public float delayAttackInterval = 0.1f;
    public float stunTime = 1.5f;
    public float percentWeak = 0.5f;
    public float weakTime = 5f;
    public float hitInvulnerableTime = 1f;
    public float skillInvulnerableTime = 3f;

    //Thêm thời gian choáng, thời gian bị yếu thời gian stun v.v...
}
[Serializable]
public class NormalBulletInfo
{
    public float speed;
    public float attackInterval;
    public float percentWeak;
    public DamageInfo myDamage;
}
[Serializable]
public class ChargeBulletInfo
{
    public float speed;
    public float chargeTime;
    public DamageInfo myDamage;
}
[Serializable]
public class ExplosionInfo
{
    public float delayTime;
    public float duration;
    public DamageInfo myDamage;
}

[Serializable]
public class SpecialAttackInfo
{
    string helo = "";
}
