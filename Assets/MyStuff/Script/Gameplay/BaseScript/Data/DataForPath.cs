﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;



[Serializable]
public class Wave
{
    public EventWave eventBefore;
    public List<Pattern> patterns;
}

[Serializable]
public class Pattern
{
    public PathCreator path;
    public List<PathCreepData> listMob;
}
[Serializable]
public class PathCreepData
{
    public Size type = Size.One;
    public bool armor = false;
    public List<ActionOnWayPoint> actionOnWayPoint;
    public float speed;
}
[Serializable]
public class NonPathCreepData
{
    public Size type = Size.One;
    public bool armor = false;
    public Vector3 distanceOffset;
    public SubMobBehaviour behaviour;
    public float deltaTheta;
}
[Serializable]
public class FollowCreepData
{
    public Size type = Size.One;
    public bool armor = false;
    public Vector2 distanceOffset;
}
/// <summary>
/// Nếu là muốn có nhiều action thì set nhiều action cho chung 1 index;
/// </summary>
[Serializable]
public class ActionOnWayPoint
{
    [Header("Hành độnh thực hiện khi tới wayPoint này")]
    public int wayPointIndex;
    public CreepAction action;
    public float value;
    public float time;
}
[Serializable]
public class StatusData
{
    public bool isInstall;
    public float delayTime;
    public bool started;
    public bool complete;

    public StatusData() { }
    public StatusData(float time)
    {
        isInstall = false;
        delayTime = time;
        started = false;
        complete = false;
    }
}
[Serializable]
public class StatusTest
{
    public PatternController pattern;
    public PathCreep pathCreep;
    public NonPathCreep nonPathCreep;

    public bool complete;
    public StatusTest() { }
    public StatusTest(NonPathCreep _target, bool finish)
    {
        nonPathCreep = _target;
        complete = finish;
    }
    public StatusTest(PathCreep _target, bool finish)
    {
        pathCreep = _target;
        complete = finish;
    }
    public StatusTest(PatternController _target, bool finish)
    {
        pattern = _target;
        complete = finish;
    }
}