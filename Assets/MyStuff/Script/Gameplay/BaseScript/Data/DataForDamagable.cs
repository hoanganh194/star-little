﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class DamageInfo
{
    public Entity damagerPreference;
    public float value;
}

