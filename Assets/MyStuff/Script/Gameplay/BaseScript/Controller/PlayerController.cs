﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    IPlayer controller;
    Side mySide;
    Dictionary<KeyAction, KeyCode> inputMap = new Dictionary<KeyAction, KeyCode>();
    bool isReady = false;
    // Start is called before the first frame update
    public void SetData(Side side, Player host)
    {
        mySide = side;
        controller = host.GetComponent<IPlayer>();
       
        switch (mySide)
        {
            case Side.Left:
                SetDataInputLeft();
                break;
            case Side.Right:
                SetDataInputRight();
                break;
        }
        if (controller != null)
        {
            isReady = true;
        }
    }
    void SetDataInputLeft()
    {
        inputMap.Clear();
        inputMap.Add(KeyAction.Up, KeyCode.T);
        inputMap.Add(KeyAction.Down, KeyCode.G);
        inputMap.Add(KeyAction.Left, KeyCode.F);
        inputMap.Add(KeyAction.Right, KeyCode.H);
        inputMap.Add(KeyAction.Shoot, KeyCode.E);
        inputMap.Add(KeyAction.Charge, KeyCode.D);
        inputMap.Add(KeyAction.Skill, KeyCode.X);
    }
    void SetDataInputRight()
    {
        inputMap.Clear();
        inputMap.Add(KeyAction.Up, KeyCode.UpArrow);
        inputMap.Add(KeyAction.Down, KeyCode.DownArrow);
        inputMap.Add(KeyAction.Left, KeyCode.LeftArrow);
        inputMap.Add(KeyAction.Right, KeyCode.RightArrow);
        inputMap.Add(KeyAction.Shoot, KeyCode.Keypad1);
        inputMap.Add(KeyAction.Charge, KeyCode.Keypad2);
        inputMap.Add(KeyAction.Skill, KeyCode.Keypad3);
    }
    // Update is called once per frame
    void Update()
    {
        if (!isReady) return;

        var move = Vector3.zero;
        if (Input.GetKey(inputMap[KeyAction.Up]))
        {
            move += Vector3.up;
        }
        if (Input.GetKey(inputMap[KeyAction.Down]))
        {
            move += Vector3.down;
        }
        if (Input.GetKey(inputMap[KeyAction.Left]))
        {
            move += Vector3.left;
        }
        if (Input.GetKey(inputMap[KeyAction.Right]))
        {
            move += Vector3.right;
        }
        if (Input.GetKey(inputMap[KeyAction.Shoot]))
        {
            controller.ShootNormal();
        }
        if (Input.GetKeyDown(inputMap[KeyAction.Charge]))
        {
            controller.ShootCharge(true);
        }
        if (Input.GetKeyUp(inputMap[KeyAction.Charge]))
        {
            controller.ShootCharge(false);
        }
        if (Input.GetKeyDown(inputMap[KeyAction.Skill]))
        {
            controller.UseSkill();
        }
        controller.Move(move);
    }
}
