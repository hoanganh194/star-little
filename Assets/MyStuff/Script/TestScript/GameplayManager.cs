﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class này đảm nhiệm việc 
/// Kiểm tra và cập nhật trạng thái của gameplay bao gồm game bắt đầu chưa, kết thúc chưa v.v
/// Chứa audioplayer để chơi audio cũng như load audio preference từ player data (trong gameplay)
/// Load thông tin màn chơi và khởi tạo WaveManager, PlayerManager và là cổng kết nối 2 thành phần quan trọng này
/// Chứa event system dùng trong gameplay => player dead, intro end, end game, change scene, custom event.
/// Chuyển giao dữ liệu của gameplay cho UI Manager ( tức là UI sẽ lấy thông tin cần thông qua nó)
/// Thứ tự thực hiện hàm :
/// Awake : instance check
/// Start : Fetch Data from Data Manager
/// 
/// </summary>
public class GameplayManager : Singleton<GameplayManager>
{
    //Danh sách nên bao gồm : WaveManager, Pool
    public Pool pool;
    public AttackManager attack;
    public PlayerManager playerManager;
    public WaveManager waveManager;
    public GameplayState gameState = GameplayState.Inactive;
    void StartTheGamePlay()
    {
        pool.StartPool();
        waveManager.PrepareWave();
        playerManager.InstallPlayer();
        attack.CreateSpecialAttackPool(playerManager.GetPlayer(Side.Left).specialAttack, playerManager.GetPlayer(Side.Right).specialAttack);
        StartCoroutine(StartCountDown());
    }
    IEnumerator StartCountDown()
    {
        for (int i = 3; i > 0; i--)
        {
            Debug.Log("Game start in: " + i);
            yield return Yielders.Get(1f);
        }
        waveManager.StartToSpawn();
    }
    private void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 100, 50), "Start the game"))
        {
            StartTheGamePlay();
            gameState = GameplayState.Pregame;
        }
    }
}
