﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Player Bullet Info", menuName = "Player/New Bullet Info", order = 2)]
public class BulletSO : ScriptableObject
{
    public string Name;
    public GameObject prefab;
    public BulletType bulletType;
    public float attackInterval;
    public string description;
    //Sau này các thông tin description sẽ nằm ở đây
}
