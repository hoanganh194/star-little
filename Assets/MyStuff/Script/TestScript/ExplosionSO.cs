﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Explosion Info", menuName = "Mob/New Explosion Info", order = 2)]
public class ExplosionSO : ScriptableObject
{
    public string Name;
    public GameObject prefab;
    public ExplosionInfo info;
    public string description;
}
