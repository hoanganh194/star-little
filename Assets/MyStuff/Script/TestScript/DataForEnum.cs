﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region GameState
public enum GameState { MainMenu, GamePlay, PostGame }
public enum GameplayState { Inactive, Pregame, Running, Pause, Endgame }
#endregion
#region EntityLayer and its behavior
    public enum EntityLayer { None, Player, Creep, Attack, Bullet, Explosion }
    #region Player
    public enum PlayerStatus { None, Normal, Charge, Stun, Skill, Hit, Death }
    public enum PlayerShootingStatus { Weak, Normal }
    public enum BulletType { None, Normal, Charge }
    #endregion
    #region Creep 
    public enum CreepType { None, PathCreep, NonPathCreep, Ghost }
    public enum Size { None, One, Two, Three, Four, Five, PowerUp }
    public enum SubMobBehaviour { Fix, RotateCounterClock, RotateNormal }
    public enum CreepAction { None, Wait, ChangeSpeed }
#endregion
    #region Attack
    public enum AttackType { None, Normal, Reverse, Special, Boss }
    public enum NormalPathType { Parabol, UpAndDown, Down, BoundTop }
    #endregion
    public enum HostDamageType { Normal, Reverse, Speical, Boss, Ghost, Creep }
    public enum EntityAnimation { Idle, Death, Hit, TurnLeft, TurnRight, Charge }
#endregion
#region Scene Position Data and Wave Infomation
public enum EventWave { None, ArmorBreak, Bomb }
public enum ItemPickUp { None, ArmorBreak, Bomb, Coin }
public enum Position { None, Left, Right, Mid }
public enum Side { None, Left, Right }
public enum Wall { Left, Right, Top, Bottom, RightOfLeft, LeftOfRight }
#endregion
#region Controller
public enum KeyAction { Up, Down, Left, Right, Shoot, Charge, Skill}
#endregion