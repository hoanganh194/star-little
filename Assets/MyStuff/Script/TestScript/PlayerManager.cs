﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class này cũng không tương đương với gameplaymanager 2 cái là riêng biệt xem chi tiết tại gameplayManager
/// class này chịu trách nhiệm spawn nhân vật và khởi tạo inventory (chứa boom) , điểm số và máu, sát thương
/// gọi hàm data để lấy thông tin về nhân vật, sát thương, tốc độ di chuyển, máu, 
/// Khởi tạo controller cho từng loại nhân vật : ví dụ local player, AI , Network player
/// Ra lệnh cho controller hoat động khi gameplay running
/// Ra lệnh cho player thực hiện animation tuỳ theo gameplay state
/// Call back báo gameend khi player dead hoặc tuỳ chỉnh 
/// Chỉ xét call back từ dưới lên ta có hàm này call với GameManager:
/// Player dead,
/// OnPickUpItem => boom , break shield
/// HP threshold
/// Use boom
/// Mỗi player manager chịu trách nhiệm cho 1 player
/// </summary>
public class PlayerManager : MonoBehaviour
{
    //Trường này sẽ xoá sau khi có DataManager
    public List<PlayerSO> playerData;
    public CallbackPlayer myCallBack = new CallbackPlayer();
    public Player leftPlayer;
    public Player rightPlayer;
    public bool isReady = false;
    public void InstallPlayer()
    {
        SettingCallBack();
        leftPlayer = Instantiate(playerData[0].prefab).GetComponent<Player>();
        leftPlayer.SetData(Side.Left);
        rightPlayer = Instantiate(playerData[0].prefab).GetComponent<Player>();
        rightPlayer.SetData(Side.Right);
    }
    public Player GetPlayer(Side side)
    {
        switch(side)
        {
            case Side.Left:
                return leftPlayer;
            case Side.Right:
                return rightPlayer;
        }
        return null;
    }
    public void OnPlayerReady()
    {
        isReady = true;
    }
    void SettingCallBack()
    {
        myCallBack.onReady = PlayerReady;
        myCallBack.onDeath = OnPlayerDeath;
    }
    void PlayerReady()
    {
        Debug.Log("Player ready");
    }
   
    void OnPlayerDeath()
    {
        Debug.Log("Player death => engame");
    }
}
