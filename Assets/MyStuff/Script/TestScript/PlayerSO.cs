﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Player Info", menuName = "Player/New Player Info", order = 1)]
public class PlayerSO : ScriptableObject
{
    public string Name;
    public GameObject prefab;
    public PlayerInfo info;
    public Sprite skillIcon;
    public string description;
    //Sau này các thông tin description sẽ nằm ở đây
}
