﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CallbackPlayerManager
{
    public Action onReady;
    public Action onSpawnBoss;
    public Action onDeath;
    public Action onUseSkill;
    public Action onChangingHP;
    public Action onGainPoint;
    public Action onPickUpItem;
    public CallbackPlayerManager()
    {
        Destroy();
    }
    public void Destroy()
    {
        onReady = null;
        onSpawnBoss = null;
        onDeath = null;
        onUseSkill = null;
        onChangingHP = null;
        onGainPoint = null;
        onPickUpItem = null;
    }
}
public class CallbackPlayer
{
    public Action onReady;
    public Action onSpawnBoss;
    public Action onDeath;
    public Action onUseSkill;// cái này có nghĩa là nhân vật sẽ hỏi cho sài skill không và nếu bên trên cho thì bên trên sẽ cho thực hiện hàm dropboom.
    public Action<float> onChangingHP;
    public Action<int> onGainPoint;
    public Action<ItemPickUp> onPickUpItem;
    public CallbackPlayer()
    {
        Destroy();
    }
    public void Destroy()
    {
        onReady = null;
        onSpawnBoss = null;
        onDeath = null;
        onUseSkill = null;
        onChangingHP = null;
        onGainPoint = null;
        onPickUpItem = null;
    }
}

