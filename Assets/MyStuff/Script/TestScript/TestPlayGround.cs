﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayGround : MonoBehaviour
{
    float maxX = 5.625f;
    float maxY = 10;

    public Mob mobTest;
    public PathCreation.PathCreator pathControl;
    public int numberOfMob = 8;
    public float delayTime = 0.2f;
    float _delay;
    private void Update()
    {

    }
    IEnumerator SpawnMob()
    {
        var startScale = 0.2f;
        for(int i = 0; i < numberOfMob; i++)
        {
            var newMob = Instantiate(mobTest);
            //newMob.SetData(Side.Left, pathControl);

            startScale += 0.2f;
            yield return new WaitForSeconds(delayTime);

        }
    }
   
	void OnDrawGizmos()
	{
		// Draw a yellow sphere at the transform's position
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireCube(Vector2.zero, new Vector2(maxX,maxY));
    }
    private void OnGUI()
    {
        if (GUI.Button(new Rect(10, 40, 130, 30), "Start"))
        {
            StartCoroutine(SpawnMob());
        }
    }
}
